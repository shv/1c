#include "E1InterfaceItem.h"

///////////////////////////////////////////////////////////////////////////////
/// E1InterfaceItem
///----------------------------------------------------------------------------
E1InterfaceItem::E1InterfaceItem(const E1InterfaceItem &item)
    : m_lIndex(item.m_lIndex),
      m_Name(item.m_Name),
      m_Component(item.m_Component)
{
}

///----------------------------------------------------------------------------
E1InterfaceItem::E1InterfaceItem(const E1ComponentObject &component, long lIndex, const QString &qsName)
    : m_lIndex(lIndex),
      m_Component(component)
{
    m_Name.insert(E1Tr::English, qsName);
}
