#include <QDebug>
#include <QException>
#include <QTimer>

#include "E1CoreApplication.h"

E1CoreApplication* E1CoreApplication::m_pSelf = NULL;

///----------------------------------------------------------------------------
E1CoreApplication &E1CoreApplication::inst()
{
    if(!m_pSelf) {
        m_pSelf = new E1CoreApplication();
        qDebug() << "E1CoreApplication::inst(): " << int(m_pSelf);
    }

    return *m_pSelf;
}

///----------------------------------------------------------------------------
void E1CoreApplication::free()
{
    if (m_pSelf) {
        qDebug() << "E1CoreApplication::free(): " << int(m_pSelf);
        delete m_pSelf;
        m_pSelf = NULL;
    }
}

///----------------------------------------------------------------------------
void E1CoreApplication::startThread(QObject* pObj)
{
    qDebug() << "E1CoreApplication::startThread() currentThread =" << int(QThread::currentThread());
    m_Mutex.lock();
    QThread* pThread = new QThread();
    qDebug() << "E1CoreApplication::startThread()" << int(pThread);
    pThread->start();

    pObj->moveToThread(pThread);

    m_Threads.insert(pObj, pThread);
    m_Mutex.unlock();
}

///----------------------------------------------------------------------------
void E1CoreApplication::stopThread(QObject *pObj)
{
    qDebug() << "E1CoreApplication::stopThread(): currentThread =" << int(QThread::currentThread());
    m_Mutex.lock();
    QMap<QObject*, QThread*>::iterator it = m_Threads.find(pObj);
    if (it == m_Threads.end()) return;

    QThread* pThread = it.value();
    pThread->quit();
    pThread->wait(100);
    if (pThread->isRunning()) {
        pThread->terminate();
        qWarning() << "E1CoreApplication::stopThread(): thread" << int(pThread) << "terminated";
    }

    m_Threads.remove(pObj);
    delete pThread;
    m_Mutex.unlock();
}

///----------------------------------------------------------------------------
E1CoreApplication::E1CoreApplication()
    : m_pCoreApp(NULL),
      m_bDeleteApp(false)
{
    QCoreApplication* pApp = QCoreApplication::instance();

    qDebug() << "E1CoreApplication::E1CoreApplication(): QCoreApplication" << int(pApp);
    if(pApp) {
        m_pCoreApp = pApp;
    } else {
        int argc = 1;
        char* argv[] = {"E1CoreApplication", NULL};
        m_pCoreApp = new QCoreApplication(argc, argv);
        m_bDeleteApp = true;
    }
}

///----------------------------------------------------------------------------
E1CoreApplication::~E1CoreApplication()
{
    qDebug() << "E1CoreApplication::~E1CoreApplication(): deleted =" << m_bDeleteApp;
    if (m_bDeleteApp) {
        // TODO: Possible memory leak
        delete m_pCoreApp;
    }
}
