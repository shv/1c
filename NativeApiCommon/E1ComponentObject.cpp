#include <QDebug>

#include "E1CoreApplication.h"
#include "E1ComponentObject.h"

///////////////////////////////////////////////////////////////////////////////
/// Public
///----------------------------------------------------------------------------
E1ComponentObject::E1ComponentObject(const QString &qsComName, long lVersion)
    : m_qsComponentName(qsComName),
      m_lVersion(lVersion),
      m_Properties(*this),
      m_Methods(*this)
{
    qWarning() << "E1ComponentObject::E1ComponentObject():";
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString& qsName, bool& bValue) const
{
    try {
        bValue = m_Properties[qsName].toBool();
        return true;

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString &qsName, bool defaultValue) const
{
    try {
        return m_Properties[qsName].toBool();
    } catch(E1Exception& /*e*/) {
        return defaultValue;
    }
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString& qsName, int& iValue) const
{
    try {
        iValue = m_Properties[qsName].toInt();
        return true;

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return false;
}

///----------------------------------------------------------------------------
int E1ComponentObject::getProperty(const QString& qsName, int defaultValue) const
{
    try {
        return m_Properties[qsName].toInt();
    } catch(E1Exception& /*e*/) {
        return defaultValue;
    }
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString& qsName, double& dValue) const
{
    try {
        dValue = m_Properties[qsName].toDouble();
        return true;

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return false;
}

///----------------------------------------------------------------------------
double E1ComponentObject::getProperty(const QString &qsName, double defaultValue) const
{
    try {
        return m_Properties[qsName].toDouble();
    } catch(E1Exception& /*e*/) {
        return defaultValue;
    }
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString& qsName, QString& qsValue) const
{
    try {
        qsValue = m_Properties[qsName].toString();
        return true;

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return false;
}

///----------------------------------------------------------------------------
QString E1ComponentObject::getProperty(const QString &qsName, QString defaultValue) const
{
    try {
        return m_Properties[qsName].toString();
    } catch(E1Exception& /*e*/) {
        return defaultValue;
    }
}

///----------------------------------------------------------------------------
bool E1ComponentObject::getProperty(const QString& qsName, E1Value& evValue) const
{
    try {
        evValue = m_Properties[qsName];
        return true;

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setProperty(const QString &qsName, bool bValue)
{
    try {
        return m_Properties[qsName].setValue(bValue, true);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }

    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setProperty(const QString &qsName, int iValue)
{
    try {
        return m_Properties[qsName].setValue(iValue, true);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }

    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setProperty(const QString &qsName, double dValue)
{
    try {
        return m_Properties[qsName].setValue(dValue, true);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }

    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setProperty(const QString &qsName, const QString &qsValue)
{
    try {
        return m_Properties[qsName].setValue(qsValue, true);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }

    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setProperty(const QString &qsName, const E1Value &evValue)
{
    try {
        return m_Properties[qsName].setValue(evValue, true);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }

    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// E1Component :: IInitDoneBase
///----------------------------------------------------------------------------
bool E1ComponentObject::Init(void* disp)
{
    qWarning() << "E1ComponentObject::Init()";
    return m_1cConnection.initConnection((IAddInDefBase*)disp);
}

///----------------------------------------------------------------------------
bool E1ComponentObject::setMemManager(void* mem)
{
    qWarning() << "E1ComponentObject::setMemManager()";
    return m_1cConnection.initMemoryManager((IMemoryManager*)mem);
}

///----------------------------------------------------------------------------
long E1ComponentObject::GetInfo()
{
    qWarning() << "E1ComponentObject::GetInfo()";
    return m_lVersion;
}

///----------------------------------------------------------------------------
void E1ComponentObject::Done()
{
    qWarning() << "E1ComponentObject::Done()";
    E1Tr::free();
}

///////////////////////////////////////////////////////////////////////////////
/// E1Component :: ILanguageExtenderBase
///----------------------------------------------------------------------------
bool E1ComponentObject::RegisterExtensionAs(WCHAR_T** wsExtensionName)
{
    qWarning() << "E1ComponentObject::RegisterExtensionAs()";
    m_1cConnection.get1CString(m_qsComponentName, wsExtensionName);
    return wsExtensionName != NULL;
}

///----------------------------------------------------------------------------
long E1ComponentObject::GetNProps()
{
    return m_Properties.count();
}

///----------------------------------------------------------------------------
long E1ComponentObject::FindProp(const WCHAR_T* wsPropName)
{
    long lResult = -1;

    try {
        lResult = m_Properties.getIndex(QString::fromUtf16((ushort*)wsPropName));

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return lResult;
}

///----------------------------------------------------------------------------
const WCHAR_T* E1ComponentObject::GetPropName(long lPropNum, long lPropAlias)
{
    WCHAR_T* pResult = NULL;

    try {
        pResult = m_1cConnection.get1CString((lPropAlias == 0) ? m_Properties.getNameEng(lPropNum) : m_Properties.getName(lPropNum));

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return pResult;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::GetPropVal(const long lPropNum, tVariant* pvarPropVal)
{
    try {
        return m_Properties.getValue(lPropNum, pvarPropVal);
    }
    catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::SetPropVal(const long lPropNum, tVariant* varPropVal)
{
    try {
        return m_Properties.setValue(lPropNum, varPropVal);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::IsPropReadable(const long lPropNum)
{
    try {
        return m_Properties.isReadable(lPropNum);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::IsPropWritable(const long lPropNum)
{
    try {
        return m_Properties.isWritable(lPropNum);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
long E1ComponentObject::GetNMethods()
{
    return m_Methods.count();
}

///----------------------------------------------------------------------------
long E1ComponentObject::FindMethod(const WCHAR_T* wsMethodName)
{
    long lResult = -1;

    try {
        lResult = m_Methods.getIndex(QString::fromUtf16((ushort*)wsMethodName));

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::NameNotFound);
    }
    return lResult;
}

///----------------------------------------------------------------------------
const WCHAR_T* E1ComponentObject::GetMethodName(const long lMethodNum, const long lMethodAlias)
{
    WCHAR_T* pResult = NULL;

    try {
        pResult = m_1cConnection.get1CString((lMethodAlias == 0) ? m_Methods.getNameEng(lMethodNum) : m_Methods.getName(lMethodNum));

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return pResult;
}

///----------------------------------------------------------------------------
long E1ComponentObject::GetNParams(const long lMethodNum)
{
    long lResult = 0;

    try {
        lResult = m_Methods.countParameters(lMethodNum);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return lResult;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::GetParamDefValue(const long lMethodNum, const long lParamNum, tVariant *pvarParamDefValue)
{
    try {
        return m_Methods.readParameterDefaultValue(lMethodNum, lParamNum, pvarParamDefValue);

    } catch(E1Exception& e) {
        tVarInit(pvarParamDefValue);

        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::HasRetVal(const long lMethodNum)
{
    try {
        return m_Methods.isFunction(lMethodNum);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::CallAsProc(const long lMethodNum, tVariant* paParams, const long lSizeArray)
{
    qDebug() << "E1ComponentObject::CallAsProc";
    try {
        return m_Methods.execute(lMethodNum, paParams, lSizeArray);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///----------------------------------------------------------------------------
bool E1ComponentObject::CallAsFunc(const long lMethodNum, tVariant* pvarRetValue, tVariant* paParams, const long lSizeArray)
{
    qDebug() << "E1ComponentObject::CallAsFunc";
    try {
        return m_Methods.execute(lMethodNum, pvarRetValue, paParams, lSizeArray);

    } catch(E1Exception& e) {
        qWarning() << e.getCode() << e.getDescription();
        m_1cConnection.addError(E1cConnection::StatusVeryImportant, getName(), e.getDescription(), e.getCode());
        assert(e.getCode() == E1Exception::OutOfRange);
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// E1Component :: LocaleBase
///----------------------------------------------------------------------------
void E1ComponentObject::SetLocale(const WCHAR_T* loc)
{
    QString qsLoc = QString::fromUtf16((ushort*)loc);

#ifndef __linux__
    wchar_t* result = ::_wsetlocale(LC_ALL, (wchar_t*) loc);

    if (qsLoc.compare("rus_RUS", Qt::CaseInsensitive) == 0 || qsLoc.compare("rus", Qt::CaseInsensitive) == 0) {
        E1Tr::inst().setLang(E1Tr::Russian);
    }
    qWarning() << "E1ComponentObject::SetLocale():" << QString::fromUtf16((ushort*) loc) << QString::fromUtf16((ushort*)result) << E1Tr::inst().getLang();
#else
// TODO:
//    int size = 0;
//    char *mbstr = 0;
//    wchar_t *tmpLoc = 0;
//    convToWchar(&tmpLoc, loc);
//    size = wcstombs(0, tmpLoc, 0)+1;
//    mbstr = new char[size];

//    if (!mbstr) {
//        delete[] tmpLoc;
//        return;
//    }

//    memset(mbstr, 0, size);
//    size = wcstombs(mbstr, tmpLoc, wcslen(tmpLoc));
//    setlocale(LC_ALL, mbstr);
//    delete[] tmpLoc;
//    delete[] mbstr;
#endif
}

///////////////////////////////////////////////////////////////////////////////
/// Protected

