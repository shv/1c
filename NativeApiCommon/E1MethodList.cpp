#include "E1MethodList.h"

///----------------------------------------------------------------------------
bool E1MethodList::execute(long lIndex, tVariant *pavParameters, long lArraySize)
{
    E1Method& method = at(lIndex);
    E1ValueList parameters(m_Component);
    inputConversion(method, parameters, pavParameters, lArraySize);

    // Execute
    bool bResult = method.execute(parameters);

    // Return changed parameters
    outputConversion(pavParameters, parameters);

    return bResult;
}

///----------------------------------------------------------------------------
bool E1MethodList::execute(long lIndex, tVariant* pvReturnValue, tVariant *pavParameters, long lArraySize)
{
    E1Method& method = at(lIndex);
    E1ValueList parameters(m_Component);
    inputConversion(method, parameters, pavParameters, lArraySize);
    E1Value returnValue(m_Component, "ret");

    // Execute
    bool bResult = method.execute(parameters, returnValue);

    // Return changed parameters and value
    outputConversion(pavParameters, parameters);
    returnValue.getValue(pvReturnValue);

    return bResult;
}

void E1MethodList::inputConversion(const E1Method &method, E1ValueList &destination, tVariant *pavSource, long lArraySize)
{
    tVariant* pvCurParameter = pavSource;
    try {
        for (int i = 0; i < lArraySize; i++) {
            E1Value& value = destination.append(method.getDefaultValue(i).getName());
            value.setValue(pvCurParameter++);
        }
    }
    catch(E1Exception& e) {
        qWarning() << e.getDescription();
        assert(false);
    }
}

void E1MethodList::outputConversion(tVariant *pavDestination, E1ValueList &source)
{
    tVariant* pvCurParameter = pavDestination;
    for (int i = 0; i < source.count(); i++) {
        source.getValue(i, pvCurParameter++);
    }
}

///----------------------------------------------------------------------------
bool E1MethodList::isFunction(long lIndex) const
{
    return at(lIndex).isFunction();
}

///----------------------------------------------------------------------------
long E1MethodList::countParameters(long lIndex) const
{
    return at(lIndex).countParameters();
}

bool E1MethodList::readParameterDefaultValue(long lIndex, long lParameterIndex, tVariant* pvDefaultValue) const
{
    return at(lIndex).getDefaultValue(lParameterIndex + 1, pvDefaultValue);
}
