#include <QDomDocument>
#include "E1EquipmentDriverAction.h"

///----------------------------------------------------------------------------
E1EquipmentDriverAction::E1EquipmentDriverAction(const E1ComponentObject &component, long lIndex, const QString& qsName)
    : E1InterfaceItem(component, lIndex, qsName),
      m_bFiresExternalEvent(false)
{
}

///----------------------------------------------------------------------------
bool E1EquipmentDriverAction::doAction(QString& qsOut)
{
    if (m_pAction == NULL) return false;
    qsOut = ((const_cast<E1ComponentObject&>(m_Component)).*m_pAction)();
    return true;
}

///----------------------------------------------------------------------------
void E1EquipmentDriverAction::makeDomChildFor(QDomElement &parent)
{
    QDomElement action = parent.ownerDocument().createElement("Action");
    action.setAttribute("Name", getName());
    action.setAttribute("Caption", getCaption());

    parent.appendChild(action);
}
