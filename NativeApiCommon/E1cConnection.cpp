#include <assert.h>
#include <QDebug>
#include <QString>
#include <QThread>

#include "E1cConnection.h"
#include "E1Component.h"
#include "E1Value.h"

///----------------------------------------------------------------------------
E1cConnection::E1cConnection()
    : m_pConnection(NULL),
      m_pMemoryManager(NULL)
{
}

///----------------------------------------------------------------------------
bool E1cConnection::initConnection(IAddInDefBase* pConnection)
{
    m_pConnection = pConnection;
    return pConnection != NULL;
}

///----------------------------------------------------------------------------
bool E1cConnection::initMemoryManager(IMemoryManager* pMemoryManager)
{
    m_pMemoryManager = pMemoryManager;
    return (pMemoryManager != NULL);
}

///////////////////////////////////////////////////////////////////////////////
/// IAddInDefBase
///----------------------------------------------------------------------------
bool E1cConnection::addError(ushort nMsgCode, const QString& qsSource, const QString& qsDescription, long lErrorCode) const
{
    return (m_pConnection == NULL) ? false : m_pConnection->AddError(nMsgCode, get1CString(qsSource), get1CString(qsDescription), lErrorCode);
}

///----------------------------------------------------------------------------
bool E1cConnection::registerStorage(const QString &qsProfileName) const
{
    return (m_pConnection == NULL) ? false : m_pConnection->RegisterProfileAs(get1CString(qsProfileName));
}

///----------------------------------------------------------------------------
bool E1cConnection::readStoredValue(const QString& qsName, E1Value& value, long& lErrorCode, QString& qsErrorDescription) const
{
    if (m_pConnection == NULL) return false;

    tVariant* pVal            = new tVariant;
    WCHAR_T*  pErrDescription = NULL;

    bool bSuccess = m_pConnection->Read(get1CString(qsName), pVal, &lErrorCode, &pErrDescription);

    if (bSuccess) {
        value.setValue(pVal);
        E1Value::clear(*this, pVal);
    }
    else {
        qsErrorDescription = QString::fromUtf16((ushort*)pErrDescription);
        freeWith1C((void**)&pErrDescription);
    }

    delete pVal;

    return bSuccess;
}

///----------------------------------------------------------------------------
bool E1cConnection::storeValue(const QString &qsName, E1Value &value) const
{
    if (m_pConnection == NULL) return false;

    tVariant* pVal = new tVariant;
    value.getValue(pVal);

    bool bResult = m_pConnection->Write(get1CString(qsName), pVal);

    delete pVal;

    return bResult;
}

///----------------------------------------------------------------------------
bool E1cConnection::externalEvent(const QString &qsSource, const QString &qsMessage, const QString &qsData) const
{
    return (m_pConnection == NULL) ? false : m_pConnection->ExternalEvent(get1CString(qsSource), get1CString(qsMessage), get1CString(qsData));
}

///----------------------------------------------------------------------------
bool E1cConnection::setEventBufferDepth(long lDepth) const
{
    qDebug() << "E1cConnection::setEventBufferDepth(): thread = " << int(QThread::currentThread());
    return (m_pConnection == NULL) ? false : m_pConnection->SetEventBufferDepth(lDepth);
}

///----------------------------------------------------------------------------
long E1cConnection::getEventBufferDepth() const
{
    return (m_pConnection == NULL) ? 0 : m_pConnection->GetEventBufferDepth();
}

///----------------------------------------------------------------------------
void E1cConnection::cleanEventBuffer() const
{
    if (m_pConnection != NULL) m_pConnection->CleanEventBuffer();
}

///----------------------------------------------------------------------------
bool E1cConnection::setStatusLine(const QString &qsStatus) const
{
    return (m_pConnection == NULL) ? false : m_pConnection->SetStatusLine(get1CString(qsStatus));
}

///----------------------------------------------------------------------------
void E1cConnection::resetStatusLine() const
{
    if (m_pConnection != NULL) m_pConnection->ResetStatusLine();
}

///////////////////////////////////////////////////////////////////////////////
/// IMsgBox
///----------------------------------------------------------------------------
bool E1cConnection::confirmDialog(const QString& qsMessage, bool& bResult) const
{
    if (m_pConnection != NULL && E1Cmp::inst().getPlatformCapabilities() >= eAppCapabilities1) {
        IAddInDefBaseEx* pCnnEx  = (IAddInDefBaseEx*) m_pConnection;
        IMsgBox*         pMsgBox = (IMsgBox*)         pCnnEx->GetInterface(eIMsgBox);
        if (pMsgBox) {
            tVariant retVal;
            tVarInit(&retVal);
            if(pMsgBox->Confirm((WCHAR_T*) qsMessage.utf16(), &retVal)) {
                bResult = TV_BOOL(&retVal);
                return true;
            }
        }
    }

    return false;
}

bool E1cConnection::alertDialog(const QString& qsMessage) const
{
    if (m_pConnection != NULL && E1Cmp::inst().getPlatformCapabilities() >= eAppCapabilities1) {
        IAddInDefBaseEx* pCnnEx  = (IAddInDefBaseEx*) m_pConnection;
        IMsgBox*         pMsgBox = (IMsgBox*)         pCnnEx->GetInterface(eIMsgBox);
        if (pMsgBox) {
            if(pMsgBox->Alert((WCHAR_T*) qsMessage.utf16())) {
                return true;
            }
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
/// IPlatformInfo
///----------------------------------------------------------------------------
QString E1cConnection::getPlatformVersion() const
{
    const IPlatformInfo::AppInfo* pAppInfo = getPlatformInfo();
    if (pAppInfo) {
        return QString::fromUtf16((ushort*) pAppInfo->AppVersion);
    }

    return "";
}

///----------------------------------------------------------------------------
IPlatformInfo::AppType E1cConnection::getApplicationType() const
{
    const IPlatformInfo::AppInfo* pAppInfo = getPlatformInfo();
    if (pAppInfo) {
        return pAppInfo->Application;
    }

    return IPlatformInfo::eAppUnknown;
}

///----------------------------------------------------------------------------
QString E1cConnection::getBrowserInfo() const
{
    const IPlatformInfo::AppInfo* pAppInfo = getPlatformInfo();
    if (pAppInfo) {
        return QString::fromUtf16((ushort*) pAppInfo->UserAgentInformation);
    }

    return "";
}

///----------------------------------------------------------------------------
const IPlatformInfo::AppInfo* E1cConnection::getPlatformInfo() const {
    if (m_pConnection != NULL && E1Cmp::inst().getPlatformCapabilities() >= eAppCapabilities1) {
        IAddInDefBaseEx* pCnnEx = (IAddInDefBaseEx*) m_pConnection;
        IPlatformInfo*   pInfo  = (IPlatformInfo*)   pCnnEx->GetInterface(eIPlatformInfo);
        if (pInfo) {
            const IPlatformInfo::AppInfo* pAppInfo = pInfo->GetPlatformInfo();
            if (pAppInfo) {
                return pAppInfo;
            }
        }
    }

    return NULL;
}

///////////////////////////////////////////////////////////////////////////////
/// IMemoryManager
///----------------------------------------------------------------------------
bool E1cConnection::allocateWith1C(void** pPtr, ulong ulBytes) const
{
    return (m_pMemoryManager == NULL) ? false : m_pMemoryManager->AllocMemory(pPtr, ulBytes);
}

///----------------------------------------------------------------------------
bool E1cConnection::freeWith1C(void** pPtr) const
{
    if (m_pMemoryManager != NULL) m_pMemoryManager->FreeMemory(pPtr);
    return (m_pMemoryManager != NULL);
}

///////////////////////////////////////////////////////////////////////////////
/// Utilities
///----------------------------------------------------------------------------
WCHAR_T* E1cConnection::get1CString(const QString &qsStr, WCHAR_T** pwsDest) const
{
    ulong iByteLen = (qsStr.size() + 1) * sizeof(WCHAR_T);
    WCHAR_T* wsStr = NULL;
    WCHAR_T** pwsStr = (pwsDest == NULL) ? pwsStr = &wsStr : pwsDest;

    if (!allocateWith1C((void**)pwsStr, iByteLen)) return NULL;

    ::memcpy((void*)*pwsStr, (void*)qsStr.utf16(), iByteLen);

    return *pwsStr;
}
