#ifndef E1EQUIPMENTDRIVERPARAMETER_H
#define E1EQUIPMENTDRIVERPARAMETER_H

#include <QStringList>
#include "E1InterfaceItem.h"
#include "E1InterfaceItem.h"

template <class T> class E1InterfaceItemList;
class E1EquipmentDriverParameterList;
class QDomElement;
class E1Property;

class E1EquipmentDriverParameter
        : public E1InterfaceItem
{
public:
    template <class T> friend class E1InterfaceItemList;
    friend class E1EquipmentDriverParameterList;

    enum Comparision {
        Equal,
        NotEqual
    };

    struct Attributes {
        QString PageCaption;
        QString GroupCaption;
        QString Description;
        QString FieldFormat;
        QMap<QString, QString> ChoiceList;
    };

    struct MasterParameter {
        Comparision Operation;
        QString     Value;
    };

    virtual ~E1EquipmentDriverParameter() {}

    void    setCaption(const QString& qsValue, int iAlias = E1Tr::English)  {m_Caption.insert(iAlias, qsValue);}
    QString getCaption()    const                                           {return E1Tr::inst().loc(m_Caption);}
    QString getCaptionEng() const                                           {return E1Tr::inst().eng(m_Caption);}

    void       setAttributes(const E1EquipmentDriverParameter::Attributes& attributes, int iAlias = E1Tr::English);
    bool       hasAttributes() const                                        {return !m_Attributes.isEmpty();}
    Attributes getAttributes() const;
    Attributes getAttributesEng() const;

    QString getTypeValue() const;

    void makeDomChildFor(QDomElement &parent);

private:
    E1EquipmentDriverParameter(const E1ComponentObject &component, long lIndex, const QString &qsName);

    E1Property*        m_pProperty;
    E1Property*        m_pMasterProperty; //@ TODO: Master parameter description

    QMap<int, QString> m_Caption;

    QMap<int, E1EquipmentDriverParameter::Attributes> m_Attributes;
};

#endif // E1EQUIPMENTDRIVERPARAMETER_H
