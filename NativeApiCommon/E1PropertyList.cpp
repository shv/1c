#include "E1PropertyList.h"

bool E1PropertyList::isWritable(long lIndex) const
{
    return at(lIndex).isWritable();
}

bool E1PropertyList::isReadable(long lIndex) const
{
    return at(lIndex).isReadable();
}

bool E1PropertyList::setValue(long lIndex, const tVariant *pValue)
{
    return at(lIndex).setValue(pValue);
}

bool E1PropertyList::getValue(long lIndex, tVariant *pValue) const
{
    return at(lIndex).getValue(pValue);
}
