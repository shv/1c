#include "E1Method.h"

///----------------------------------------------------------------------------
E1Method::E1Method(const E1ComponentObject &component, long lIndex, const QString &qsName)
    : E1InterfaceItem(component, lIndex, qsName),
      m_defaultParameters(component),
      m_pFunction(NULL),
      m_pProcedure(NULL)
{
}

///----------------------------------------------------------------------------
void E1Method::setProcedure(interface_procedure pMethod)
{
    m_pFunction = NULL;
    m_pProcedure = pMethod;
}

///----------------------------------------------------------------------------
void E1Method::setFunction(interface_function pMethod)
{
    m_pFunction = pMethod;
    m_pProcedure = NULL;
}

///----------------------------------------------------------------------------
E1Value& E1Method::addParameter(const QString &qsName)
{
    return m_defaultParameters.append(qsName.isEmpty() ? "par" + m_defaultParameters.count() : qsName);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName,const tVariant *pDefault)
{
//    qWarning();
    return addParameter(qsName).setValue(pDefault);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, int32_t iDefault)
{
    return addParameter(qsName).setValue(iDefault);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, double dDefault)
{
    return addParameter(qsName).setValue(dDefault);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, bool bDefault)
{
    return addParameter(qsName).setValue(bDefault);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, int year, int month, int day, int hour, int min, int sec)
{
    return addParameter(qsName).setValue(year, month, day, hour, min, sec);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, const char *pValue)
{
    return addParameter(qsName).setValue(pValue);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, const QByteArray &aDefault)
{
    return addParameter(qsName).setValue(aDefault);
}

///----------------------------------------------------------------------------
bool E1Method::addParameter(const QString &qsName, const QString &qsDefault)
{
    return addParameter(qsName).setValue(qsDefault);
}

///----------------------------------------------------------------------------
bool E1Method::getDefaultValue(long lIndex, tVariant *pvDefaultValue) const
{
    try {
        return m_defaultParameters.at(lIndex).getValue(pvDefaultValue);
    }
    catch(E1Exception& e) {
        if (e.getCode() != E1Exception::OutOfRange) {
            qWarning() << e.getDescription();
            assert(false);
        }
        tVarInit(pvDefaultValue);
    }

    return false;
}

///----------------------------------------------------------------------------
const E1Value &E1Method::getDefaultValue(long lIndex) const
{
    try {
        return m_defaultParameters.at(lIndex);
    }
    catch(E1Exception& e) {
        qWarning() << e.getDescription();
        assert(false);
    }

    return *(E1Value*)NULL; // Dummy: warning
}

///----------------------------------------------------------------------------
bool E1Method::getDefaultValue(const QString &qsName, tVariant *pvDefaultValue) const
{
    try {
        return m_defaultParameters.at(qsName).getValue(pvDefaultValue);
    }
    catch(E1Exception& e) {
        if (e.getCode() != E1Exception::NameNotFound) {
            qWarning() << e.getDescription();
            assert(false);
        }
        tVarInit(pvDefaultValue);
    }

    return false;
}

///----------------------------------------------------------------------------
const E1Value &E1Method::getDefaultValue(const QString &qsName) const
{
    try {
        return m_defaultParameters.at(qsName);
    }
    catch(E1Exception& e) {
        qWarning() << e.getDescription();
        assert(false);
    }

    return *(E1Value*) NULL;
}

///----------------------------------------------------------------------------
bool E1Method::execute(E1ValueList& parameters) const
{
    qDebug() << "E1Method::execute(procedure)" << getName();
    if (m_pProcedure == NULL) return false;
    ((const_cast<E1ComponentObject&>(m_Component)).*m_pProcedure)(*this, parameters);
    return true;
}

///----------------------------------------------------------------------------
bool E1Method::execute(E1ValueList& parameters, E1Value&returnValue) const
{
    qDebug() << "E1Method::execute(function)" << getName();
    if (m_pFunction == NULL) return false;
    ((const_cast<E1ComponentObject&>(m_Component)).*m_pFunction)(*this, parameters, returnValue);
    return true;
}
