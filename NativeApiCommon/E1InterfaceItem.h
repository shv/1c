#ifndef E1INTERFACEITEM_H
#define E1INTERFACEITEM_H

#include <QDebug>
#include <QVector>
#include <QString>
#include <assert.h>

#include "E1Exception.h"
#include "E1Translator.h"

template <class T> class E1InterfaceItemList;
class E1ComponentObject;

class E1InterfaceItem
{
public:
    template <class T> friend class E1InterfaceItemList;

    virtual ~E1InterfaceItem() {}

    long getIndex() const {return m_lIndex;}

    void    setName(const QString& qsName, int iAlias) {assert(!m_Name.contains(iAlias)); m_Name.insert(iAlias, qsName);}
    QString getName() const                            {return E1Tr::inst().loc(m_Name);}
    QString getNameEng() const                         {return E1Tr::inst().eng(m_Name);}

protected:
    E1InterfaceItem(const E1InterfaceItem &item);
    E1InterfaceItem(const E1ComponentObject& component, long lIndex, const QString &qsName);

    const E1ComponentObject& m_Component;

private:
    long               m_lIndex;
    QMap<int, QString> m_Name;
};

#endif // E1INTERFACEITEM_H
