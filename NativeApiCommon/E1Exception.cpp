#include "E1Exception.h"

///----------------------------------------------------------------------------
E1Exception::E1Exception(const E1Exception &e)
    : m_iCode(e.m_iCode),
      m_Description(e.m_Description)
{
}

///----------------------------------------------------------------------------
E1Exception::E1Exception(int iCode, const QString &qsDescriptionEn)
    : m_iCode(iCode)
{
    m_Description.insert(E1Tr::English, qsDescriptionEn);
}

///----------------------------------------------------------------------------
void E1Exception::clear()
{
    m_iCode = Unknown;
    m_Description.clear();
    m_Description.insert(E1Tr::English, "");
}

///----------------------------------------------------------------------------
E1Exception &E1Exception::operator =(const E1Exception &right)
{
    if (this == &right) return *this;

    m_iCode = right.m_iCode;
    m_Description = right.m_Description;

    return *this;
}
