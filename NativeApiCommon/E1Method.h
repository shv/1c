#ifndef E1METHOD_H
#define E1METHOD_H

#include "E1Value.h"
#include "E1ValueList.h"

template <class T> class E1InterfaceItemList;
class E1MethodList;
class E1Method;

typedef void (E1ComponentObject::*interface_function)(const E1Method&, E1ValueList&, E1Value&);
typedef void (E1ComponentObject::*interface_procedure)(const E1Method&, E1ValueList&);

class E1Method
        : public E1InterfaceItem
{
public:
    template <class T> friend class E1InterfaceItemList;
    friend class E1MethodList;

    virtual ~E1Method() {}

    void setProcedure(interface_procedure pMethod);
    void setFunction(interface_function pMethod);

    bool isFunction() const {return m_pFunction != NULL;}
    long countParameters() const {return m_defaultParameters.count();}

    E1Value& addParameter(const QString &qsName = "");
    bool addParameter(const QString &qsName, const tVariant* pDefault);
    bool addParameter(const QString &qsName, int32_t iDefault);
    bool addParameter(const QString &qsName, double dDefault);
    bool addParameter(const QString &qsName, bool bDefault);
    bool addParameter(const QString &qsName, int year, int month, int day, int hour = 0, int min = 0, int sec = 0);
    bool addParameter(const QString &qsName, const char* pValue);
    bool addParameter(const QString &qsName, const QByteArray& aDefault);
    bool addParameter(const QString &qsName, const QString& qsDefault);

    bool           getDefaultValue(long lIndex, tVariant *pvDefaultValue) const;
    const E1Value& getDefaultValue(long lIndex) const;
    bool           getDefaultValue(const QString &qsName, tVariant *pvDefaultValue) const;
    const E1Value& getDefaultValue(const QString &qsName) const;

protected:
    E1ValueList         m_defaultParameters;
    interface_function  m_pFunction;
    interface_procedure m_pProcedure;

private:
    E1Method(const E1ComponentObject& component, long lIndex, const QString &qsName);

    bool execute(E1ValueList &parameters) const;                        // !const_cast!
    bool execute(E1ValueList &parameters, E1Value &returnValue) const;  // !const_cast!
};

#endif // E1METHOD_H
