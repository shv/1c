#ifndef E1COMPONENTOBJECT_H
#define E1COMPONENTOBJECT_H

#include <QObject>
#include <QThread>

#include "ComponentBase.h"
#include "AddInDefBase.h"
#include "E1cConnection.h"

#include "E1PropertyList.h"
#include "E1MethodList.h"

#include "E1Exception.h"
#include "E1Translator.h"

class E1ComponentObject
        : public IComponentBase
{
public:
    E1ComponentObject(const QString &qsComName, long lVersion = 0);
    virtual ~E1ComponentObject() {}

    const QString& getName() const    {return m_qsComponentName;}
    long           getVersion() const {return m_lVersion;}

    bool getProperty(const QString& qsName, bool&    bValue)  const;
    bool getProperty(const QString& qsName, int&     iValue)  const;
    bool getProperty(const QString& qsName, double&  dValue)  const;
    bool getProperty(const QString& qsName, QString& qsValue) const;
    bool getProperty(const QString& qsName, E1Value& evValue) const;

    bool    getProperty(const QString& qsName, bool    defaultValue) const;
    int     getProperty(const QString& qsName, int     defaultValue) const;
    double  getProperty(const QString& qsName, double  defaultValue) const;
    QString getProperty(const QString& qsName, QString defaultValue) const;

    bool setProperty(const QString& qsName, bool    bValue);
    bool setProperty(const QString& qsName, int     iValue);
    bool setProperty(const QString& qsName, double  dValue);
    bool setProperty(const QString& qsName, const QString& qsValue);
    bool setProperty(const QString& qsName, const E1Value& evValue);

    bool allocateWith1C(void** pPtr, ulong ulBytes) const {return m_1cConnection.allocateWith1C(pPtr, ulBytes);}
    bool freeWith1C(void** pPtr) const                    {return m_1cConnection.freeWith1C(pPtr);}

    // IInitDoneBase
    virtual bool ADDIN_API Init(void* disp);
    virtual bool ADDIN_API setMemManager(void* mem);
    virtual long ADDIN_API GetInfo();
    virtual void ADDIN_API Done();

    // ILanguageExtenderBase
    virtual bool ADDIN_API RegisterExtensionAs(WCHAR_T**);
    virtual long ADDIN_API GetNProps();
    virtual long ADDIN_API FindProp(const WCHAR_T* wsPropName);
    virtual const WCHAR_T* ADDIN_API GetPropName(long lPropNum, long lPropAlias);
    virtual bool ADDIN_API GetPropVal(const long lPropNum, tVariant* pvarPropVal);
    virtual bool ADDIN_API SetPropVal(const long lPropNum, tVariant* varPropVal);
    virtual bool ADDIN_API IsPropReadable(const long lPropNum);
    virtual bool ADDIN_API IsPropWritable(const long lPropNum);
    virtual long ADDIN_API GetNMethods();
    virtual long ADDIN_API FindMethod(const WCHAR_T* wsMethodName);
    virtual const WCHAR_T* ADDIN_API GetMethodName(const long lMethodNum, const long lMethodAlias);
    virtual long ADDIN_API GetNParams(const long lMethodNum);
    virtual bool ADDIN_API GetParamDefValue(const long lMethodNum, const long lParamNum, tVariant* pvarParamDefValue);
    virtual bool ADDIN_API HasRetVal(const long lMethodNum);
    virtual bool ADDIN_API CallAsProc(const long lMethodNum, tVariant* paParams, const long lSizeArray);
    virtual bool ADDIN_API CallAsFunc(const long lMethodNum, tVariant* pvarRetValue, tVariant* paParams, const long lSizeArray);

    // ILocaleBase
    virtual void ADDIN_API SetLocale(const WCHAR_T* loc);

protected:
    E1cConnection   m_1cConnection;

    const QString   m_qsComponentName;
    const long      m_lVersion;

    E1PropertyList  m_Properties;
    E1MethodList    m_Methods;

private:
};

#define DATE_VALUE(...) __VA_ARGS__

#define ADD_PROPERTY(_name, _name_eng, _default_value)          \
    E1Property& _name = m_Properties.append(_name_eng);         \
    _name.setValue(_default_value)

#define ADD_PROPERTY_RO(_name, _name_eng, _default_value)       \
    ADD_PROPERTY(_name, _name_eng, _default_value);             \
    _name.setWritable(false)

#define ADD_PROPERTY_WO(_name, _name_eng, _default_value)       \
    ADD_PROPERTY(_name, _name_eng, _default_value);             \
    _name.setReadable(false)

#define RUS_PROPERTY(_name, _name_loc)                          \
    m_Properties.setItemName(_name, _name_loc, E1Tr::Russian)

#define ADD_E1_FUNCTION(_class, _member, _name_eng)             \
    E1Method& _member = m_Methods.append(_name_eng);            \
    _member.setFunction((interface_function)&_class::_member)

#define RUS_FUNCTION(_member, _name_loc)                        \
    m_Methods.setItemName(_member, _name_loc, E1Tr::Russian)

#define ADD_E1_PROCEDURE(_class, _member, _name_eng)            \
    E1Method& _member = m_Methods.append(_name_eng);            \
    _member.setProcedure((interface_procedure)&_class::_member)

#define RUS_PROCEDURE(_member, _name_loc) RUS_FUNCTION(_member, _name_loc)

#define ADD_PARAMETER(_member, _name, _default_value) _member.addParameter(_name, _default_value)

#endif // E1COMPONENTOBJECT_H
