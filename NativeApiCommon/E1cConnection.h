#ifndef E1CCONNECTION_H
#define E1CCONNECTION_H

#include <QString>
#include "IMemoryManager.h"
#include "AddInDefBase.h"

class E1Value;
class E1cConnection;

typedef E1cConnection E1C;

class E1cConnection
{
public:
    E1cConnection();

    bool initConnection(IAddInDefBase* pConnection);
    bool initMemoryManager(IMemoryManager* pMemoryManager);

    static const ushort StatusNone          = ADDIN_E_NONE;
    static const ushort StatusOrdinary      = ADDIN_E_ORDINARY;
    static const ushort StatusAttention     = ADDIN_E_ATTENTION;
    static const ushort StatusImportant     = ADDIN_E_IMPORTANT;
    static const ushort StatusVeryImportant = ADDIN_E_VERY_IMPORTANT;
    static const ushort StatusInformation   = ADDIN_E_INFO;
    static const ushort MsgBoxAttention     = ADDIN_E_MSGBOX_ATTENTION;
    static const ushort MsgBoxInformation   = ADDIN_E_MSGBOX_INFO;
    static const ushort MsgBoxError         = ADDIN_E_MSGBOX_FAIL;

    // IAddInDefBase
    bool addError(ushort nMsgCode, const QString& qsSource, const QString& qsDescription, long lErrorCode = 0) const;

    bool registerStorage(const QString& qsProfileName) const;
    bool readStoredValue(const QString& qsName, E1Value& value, long& lErrorCode, QString& qsErrorDescription) const;
    bool storeValue(     const QString& qsName, E1Value& value) const;

    bool externalEvent(const QString& qsSource, const QString& qsMessage, const QString& qsData) const;
    bool setEventBufferDepth(long lDepth) const;
    long getEventBufferDepth() const;
    void cleanEventBuffer() const;

    bool setStatusLine(const QString& qsStatus) const;
    void resetStatusLine() const;

    // IMsgBox
    bool confirmDialog(const QString& qsMessage, bool& bResult) const;
    bool alertDialog(  const QString& qsMessage) const;

    // IPlatformInfo
    QString                getPlatformVersion() const;
    IPlatformInfo::AppType getApplicationType() const;
    QString                getBrowserInfo() const;

    // IMemoryManager
    bool allocateWith1C(void** pPtr, ulong ulBytes) const;
    bool freeWith1C(    void** pPtr) const;

    // Utilities
    WCHAR_T* get1CString(const QString &qsStr, WCHAR_T** pwsDest = NULL) const;

private:
    const IPlatformInfo::AppInfo* getPlatformInfo() const;

    IAddInDefBase*    m_pConnection;
    IMemoryManager*   m_pMemoryManager;
};

#endif // E1CCONNECTION_H
