#ifndef E1INTERFACEITEMLIST_H
#define E1INTERFACEITEMLIST_H

#include <QMap>
#include <QMultiMap>
#include <QString>
#include <assert.h>

#include "E1InterfaceItem.h"
#include "E1Exception.h"

///////////////////////////////////////////////////////////////////////////////
template <class T>
class E1InterfaceItemList
{
public:
    E1InterfaceItemList(const E1ComponentObject& component) : m_Component(component) {}
    virtual ~E1InterfaceItemList();

    virtual T& append(const QString &qsName);
    virtual void setItemName(T& item, const QString &qsName, int iAlias);

    long    getIndex(const QString &qsName) const;
    QString getName(long lIndex) const;
    QString getNameEng(long lIndex) const;
    long    count() const {return m_MapByIndex.size();}

    T& at(long lIndex);
    T& at(const QString &qsName);
    const T& at(long lIndex) const;
    const T& at(const QString &qsName) const;

    T& operator[](long lIndex);
    T& operator[](const QString &qsName);
    const T& operator[](long lIndex) const;
    const T& operator[](const QString &qsName) const;

protected:
    QMap<long, T*>         m_MapByIndex;
    QMultiMap<QString, T*> m_MapByName;

    const E1ComponentObject&     m_Component;
};

///////////////////////////////////////////////////////////////////////////////
/// Implementation
///----------------------------------------------------------------------------
template <class T>
E1InterfaceItemList<T>::~E1InterfaceItemList()
{
    QMap<long, T*>::iterator it = m_MapByIndex.begin();
    for (; it != m_MapByIndex.end(); ++it) delete it.value();
}

///----------------------------------------------------------------------------
template <class T>
T& E1InterfaceItemList<T>::append(const QString &qsName)
{
    assert(!m_MapByName.contains(qsName));

    long lNewIndex = m_MapByIndex.size();
    T* pItem = new T(m_Component, lNewIndex, qsName);

    m_MapByIndex.insert(lNewIndex, pItem);
    m_MapByName.insert(qsName, pItem);

    return *pItem;
}

///----------------------------------------------------------------------------
template <class T>
void E1InterfaceItemList<T>::setItemName(T& item, const QString &qsName, int iAlias)
{
    assert(m_MapByName.find(item.getNameEng(), &item) != m_MapByName.end());

    item.setName(qsName, iAlias);
    m_MapByName.insert(qsName, &item);
}

///----------------------------------------------------------------------------
template <class T>
long E1InterfaceItemList<T>::getIndex(const QString &qsName) const
{
    return at(qsName).getIndex();
}

///----------------------------------------------------------------------------
template <class T>
QString E1InterfaceItemList<T>::getName(long lIndex) const
{
    return at(lIndex).getName();
}

///----------------------------------------------------------------------------
template <class T>
QString E1InterfaceItemList<T>::getNameEng(long lIndex) const
{
    return at(lIndex).getNameEng();
}

///----------------------------------------------------------------------------
template <class T>
T& E1InterfaceItemList<T>::at(long lIndex)
{
    QMap<long, T*>::iterator it = m_MapByIndex.find(lIndex);
    if (it == m_MapByIndex.end()){
        EX_INIT(QString("Index %1 not found").arg(lIndex), E1Exception::OutOfRange);
        EX_RUS( QString("Индекс %1 не найден").arg(lIndex));
        throw EX;
    }
    return *(it.value());
}

///----------------------------------------------------------------------------
template <class T>
const T& E1InterfaceItemList<T>::at(long lIndex) const
{
    QMap<long, T*>::const_iterator it = m_MapByIndex.find(lIndex);
    if (it == m_MapByIndex.end()) {
        EX_INIT(QString("Index %1 not found").arg(lIndex), E1Exception::OutOfRange);
        EX_RUS( QString("Индекс %1 не найден").arg(lIndex));
        throw EX;
    }
    return *(it.value());
}

///----------------------------------------------------------------------------
template <class T>
T& E1InterfaceItemList<T>::at(const QString &qsName)
{
    QMultiMap<QString, T*>::iterator it = m_MapByName.find(qsName);
    if (it == m_MapByName.end()) {
        EX_INIT(QString("Name \"%1\" not found").arg(qsName), E1Exception::NameNotFound);
        EX_RUS( QString("Имя \"%1\" не найдено").arg(qsName));
        throw EX;
    }
    return *(it.value());
}

///----------------------------------------------------------------------------
template <class T>
const T& E1InterfaceItemList<T>::at(const QString &qsName) const
{
    QMultiMap<QString, T*>::const_iterator it = m_MapByName.find(qsName);
    if (it == m_MapByName.end()) {
        EX_INIT(QString("Name \"%1\" not found").arg(qsName), E1Exception::NameNotFound);
        EX_RUS( QString("Имя \"%1\" не найдено").arg(qsName));
        throw EX;
    }
    return *(it.value());
}

///----------------------------------------------------------------------------
template <class T>
T& E1InterfaceItemList<T>::operator [](long lIndex)
{
    return at(lIndex);
}

///----------------------------------------------------------------------------
template <class T>
const T& E1InterfaceItemList<T>::operator [](long lIndex) const
{
    return at(lIndex);
}

///----------------------------------------------------------------------------
template <class T>
T& E1InterfaceItemList<T>::operator [](const QString &qsName)
{
    return at(qsName);
}

///----------------------------------------------------------------------------
template <class T>
const T& E1InterfaceItemList<T>::operator [](const QString &qsName) const
{
    return at(qsName);
}

#endif // E1INTERFACEITEMLIST_H
