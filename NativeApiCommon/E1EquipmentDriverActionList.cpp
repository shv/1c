#include <QDomDocument>
#include "E1EquipmentDriverActionList.h"

///----------------------------------------------------------------------------
QString E1EquipmentDriverActionList::xml(int indent)
{
    QDomDocument doc;
    QDomProcessingInstruction pi = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\" ");
    doc.insertBefore(pi, QDomNode());

    QDomElement list = doc.createElement("Actions");
    doc.appendChild(list);

    for (int i = 0, limit = count(); i < limit; i++) {
        at(i).makeDomChildFor(list);
    }

    return doc.toString(indent);
}


