QT       -= gui
QT       += xml

TARGET = NativeApiCommon
TEMPLATE = lib

CONFIG += staticlib
CONFIG += console

CONFIG(release, debug|release){
#    DEFINES += QT_NO_DEBUG_OUTPUT

    win32-msvc* {
        QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
    }
}

DEFINES += _WINDOWS
DEFINES += NATIVEAPI_QT_LIBRARY

win32-msvc* {
    DEFINES += "_BIND_TO_CURRENT_VCLIBS_VERSION=1"
}

INCLUDEPATH += "C:\Git\1C\NativeAPI"

SOURCES += \
    E1EquipmentDriver.cpp \
    E1Exception.cpp \
    E1InterfaceItem.cpp \
    E1Method.cpp \
    E1MethodList.cpp \
    E1Property.cpp \
    E1PropertyList.cpp \
    E1Translator.cpp \
    E1Value.cpp \
    E1ValueList.cpp \
    E1CoreApplication.cpp \
    E1cConnection.cpp \
    E1EquipmentDriverAction.cpp \
    E1EquipmentDriverActionList.cpp \
    E1EquipmentDriverParameter.cpp \
    E1EquipmentDriverParameterList.cpp \
    E1ComponentObject.cpp \
    E1Component.cpp

HEADERS += \
    E1EquipmentDriver.h \
    E1Exception.h \
    E1InterfaceItem.h \
    E1InterfaceItemList.h \
    E1Method.h \
    E1MethodList.h \
    E1Property.h \
    E1PropertyList.h \
    E1Translator.h \
    E1Value.h \
    E1ValueList.h \
    E1CoreApplication.h \
    E1cConnection.h \
    E1EquipmentDriverAction.h \
    E1EquipmentDriverActionList.h \
    E1EquipmentDriverParameter.h \
    E1EquipmentDriverParameterList.h \
    E1ComponentObject.h \
    E1Component.h \
    E1ComponentObjectFactory.h
