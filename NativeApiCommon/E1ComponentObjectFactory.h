#ifndef E1COMPONENTOBJECTFACTORY_H
#define E1COMPONENTOBJECTFACTORY_H

#include <QString>

#include "E1ComponentObject.h"

// This interface should be implemented in main project
class E1ComponentObjectFactory
{
public:
    virtual ~E1ComponentObjectFactory() {}

    virtual QVector<QString> getClasses() const = 0;
    virtual E1ComponentObject* makeObject(const QString& qsClassName) const = 0;
};

// This function should be implemented in main project
E1ComponentObjectFactory* getComponentFactory();

#endif // E1COMPONENTOBJECTFACTORY_H
