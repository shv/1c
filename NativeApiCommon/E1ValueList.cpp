#include "E1ValueList.h"
#include "E1ComponentObject.h"

///----------------------------------------------------------------------------
bool E1ValueList::setValue(long lIndex, const tVariant *pValue)
{
    return at(lIndex).setValue(pValue);
}

///----------------------------------------------------------------------------
bool E1ValueList::getValue(long lIndex, tVariant *pValue) const
{
    return at(lIndex).getValue(pValue);
}

///----------------------------------------------------------------------------
E1Value& E1ValueList::append(const QString &qsName)
{
    assert(!m_MapByName.contains(qsName));

    long lNewIndex = m_MapByIndex.size();
    E1Value* pItem = new E1Value(m_Component, lNewIndex, qsName);

    m_MapByIndex.insert(lNewIndex, pItem);
    m_MapByName.insert(qsName, pItem);

    return *pItem;
}
