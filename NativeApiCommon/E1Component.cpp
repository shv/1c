#include <assert.h>
#include <QDebug>

#include "E1Component.h"
#include "E1CoreApplication.h"

E1Component* E1Component::m_pSelf = NULL;

/////////////////////////////////////////////////////////////////////////////////
///// Load and create component
///----------------------------------------------------------------------------
const WCHAR_T* GetClassNames()
{
    qDebug() << "::GetClassNames()" << "currentThreadId =" << QThread::currentThreadId();
    return (WCHAR_T*) E1Cmp::inst().getClassNames().utf16();
}

///----------------------------------------------------------------------------
long GetClassObject(const WCHAR_T* wsName, IComponentBase** pInterface)
{
    qDebug() << "::GetClassObject(" << QString::fromUtf16(wsName) << "," << long(pInterface) << ")" << "currentThread =" << int(QThread::currentThread());
    if(*pInterface) return 0;

    *pInterface = (IComponentBase*) E1Cmp::inst().makeClassObject(QString::fromUtf16(wsName));
    if (*pInterface) E1Cmp::inst().onObjectCreated();

    return (long) *pInterface;
}

///----------------------------------------------------------------------------
long DestroyObject(IComponentBase** pIntf)
{
    qDebug() << "::DestroyObject(" << long(pIntf) << ")" << "currentThread =" << int(QThread::currentThread());
    if(!*pIntf) return -1;

    E1ComponentObject* ptr = (E1ComponentObject*)*pIntf;
    delete ptr;
    *pIntf = NULL;

    E1Cmp::inst().onObjectDestroyed();

    return 0;
}

///----------------------------------------------------------------------------
AppCapabilities SetPlatformCapabilities(const AppCapabilities capabilities)
{
    qDebug() << "::SetPlatformCapabilities(" << capabilities << ")";
    E1Cmp::inst().setPlatformCapabilities(capabilities);
    return eAppCapabilitiesLast;
}


///////////////////////////////////////////////////////////////////////////////
/// E1Component
///----------------------------------------------------------------------------
E1Component&E1Component::inst()
{
    if(!m_pSelf) {
        m_pSelf = new E1Component();
        qDebug() << "E1Component::inst(): " << int(m_pSelf);
    }

    return *m_pSelf;
}

///----------------------------------------------------------------------------
void E1Component::free()
{
    if (m_pSelf) {
        qDebug() << "E1Component::free() " << int(m_pSelf);
        delete m_pSelf;
        m_pSelf = NULL;
    }
}

///----------------------------------------------------------------------------
const QString& E1Component::getClassNames()
{
    qDebug() << "E1Component::getClassNames():";
    E1ComponentObjectFactory* pFactory =
            m_pFactory == NULL ? getComponentFactory() : m_pFactory;

    static QString qsNames("");
    QVector<QString> names = pFactory->getClasses();
    for (int i = 0; i < names.size(); ++i) {
        qsNames += "|" + names.at(i);
    }
    qsNames = qsNames.mid(1);

    if (m_pFactory == NULL) delete pFactory;

    return qsNames;
}

///----------------------------------------------------------------------------
E1ComponentObject* E1Component::makeClassObject(const QString& qsName)
{
    qDebug() << "E1Component::makeClassObject():" << qsName;
    if (m_pFactory == NULL) m_pFactory = getComponentFactory();

    E1ComponentObject* pObject = m_pFactory->makeObject(qsName);
    if (!pObject && !m_iObjectCount) {
        delete m_pFactory;
        m_pFactory = NULL;
    }

    return pObject;
}

///----------------------------------------------------------------------------
void E1Component::onObjectCreated()
{
    qDebug() << "E1Component::onObjectCreated():";
    ++m_iObjectCount;
}

///----------------------------------------------------------------------------
void E1Component::onObjectDestroyed()
{
    qDebug() << "E1Component::onObjectDestroyed():";
    --m_iObjectCount;

    if (!m_iObjectCount) {
        E1Tr::free();
        E1App::free();

        if (m_pFactory != NULL) {
            delete m_pFactory;
            m_pFactory = NULL;
        }
    }
}
