#include <QDomDocument>
#include "E1Property.h"
#include "E1EquipmentDriverParameterList.h"

///----------------------------------------------------------------------------
E1EquipmentDriverParameterList::E1EquipmentDriverParameterList(const E1ComponentObject &component)
    : E1InterfaceItemList(component)
{
}

///----------------------------------------------------------------------------
E1EquipmentDriverParameter &E1EquipmentDriverParameterList::append(E1Property& property, const QString qsCaption)
{
    QString qsName(property.getNameEng());

    assert(!m_MapByName.contains(qsName));

    long lNewIndex = m_MapByIndex.size();
    E1EquipmentDriverParameter* pItem = new E1EquipmentDriverParameter(m_Component, lNewIndex, qsName);

    m_MapByIndex.insert(lNewIndex, pItem);
    m_MapByName.insert(qsName, pItem);

    pItem->m_pProperty = &property;
    pItem->setCaption(qsCaption);

    return *pItem;
}

///----------------------------------------------------------------------------
QString E1EquipmentDriverParameterList::xml(int indent)
{
    QDomDocument doc;
    QDomProcessingInstruction pi = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\" ");
    doc.insertBefore(pi, QDomNode());

    QDomElement list = doc.createElement("Settings");
    doc.appendChild(list);

    TR_INIT("Parameters");
    TR_RUS( "Параметры");
    QString qsDefaultPage(TR);

    QDomElement page = doc.createElement("Page");
    page.setAttribute("Caption", qsDefaultPage);

    QMap<QString, QDomElement> pages;
    pages.insert(qsDefaultPage, page);

    for (int i = 0, limit = count(); i < limit; i++) {
        E1EquipmentDriverParameter& parameter      = at(i);
        QString                     qsPageCaption  = qsDefaultPage;
        QString                     qsGroupCaption = "";

        if (parameter.hasAttributes()) {
            E1EquipmentDriverParameter::Attributes att = parameter.getAttributes();

            if (!att.PageCaption.isEmpty())  qsPageCaption = att.PageCaption;

            if (!att.GroupCaption.isEmpty()) qsGroupCaption = att.GroupCaption;
        }

        // Page
        QMap<QString, QDomElement>::iterator it = pages.find(qsPageCaption);
        if (it == pages.end()) {
            page = doc.createElement("Page");
            page.setAttribute("Caption", qsPageCaption);
            pages.insert(qsPageCaption, page);
        }
        else {
            page = it.value();
        }

        QDomElement parent(page);

        // Group
        if (!qsGroupCaption.isEmpty()) {
            QDomElement group;
            QDomNodeList groups = page.elementsByTagName("Group");
            for (int j = 0, groupsCount = groups.count(); j < groupsCount; j++) {
                QDomElement curGroup = groups.at(j).toElement();
                if (curGroup.attribute("Caption") == qsGroupCaption) {
                    group = curGroup;
                    break;
                }
            }

            if (group.isNull()) {
                group = doc.createElement("Group");
                group.setAttribute("Caption", qsGroupCaption);

                page.appendChild(group);
            }

            parent = group;
        }

        parameter.makeDomChildFor(parent);
    }

    QMap<QString, QDomElement>::const_iterator it = pages.cbegin();
    for (; it != pages.cend(); ++it) {
        if (it.value().hasChildNodes()) list.appendChild(it.value());
    }

    return doc.toString(indent);
}
