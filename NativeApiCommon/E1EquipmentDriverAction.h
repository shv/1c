#ifndef E1EQUIPMENTDRIVERACTION_H
#define E1EQUIPMENTDRIVERACTION_H

#include "E1InterfaceItem.h"

template <class T> class E1InterfaceItemList;
class QDomElement;

typedef QString (E1ComponentObject::*action_function)();

class E1EquipmentDriverAction
        : public E1InterfaceItem
{
public:
    template <class T> friend class E1InterfaceItemList;

    void    setCaption(const QString& qsValue, int iAlias = E1Tr::English) {m_Caption.insert(iAlias, qsValue);}
    QString getCaption()    const                                          {return E1Tr::inst().loc(m_Caption);}
    QString getCaptionEng() const                                          {return E1Tr::inst().eng(m_Caption);}

    void setAction(action_function pAction) {m_pAction = pAction;}
    bool doAction(QString &qsOut); // !const_cast!

    void firesExternalEvent(bool bValue) {m_bFiresExternalEvent = bValue;}
    bool firesExternalEvent() const      {return m_bFiresExternalEvent;}

    void makeDomChildFor(QDomElement &parent);

private:
    E1EquipmentDriverAction(const E1ComponentObject &component, long lIndex, const QString& qsName);

    QMap<int, QString> m_Caption;
    action_function    m_pAction;

    bool               m_bFiresExternalEvent;
};

#endif // E1EQUIPMENTDRIVERACTION_H
