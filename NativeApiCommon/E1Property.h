#ifndef E1PROPERTY_H
#define E1PROPERTY_H

#include "E1Value.h"
#include "types.h"

template <class T> class E1InterfaceItemList;

class E1Property
        : public E1Value
{
public:
    template <class T> friend class E1InterfaceItemList;

    virtual ~E1Property() {}

    bool isWritable() const             {return m_bWritable;}
    void setWritable(bool value = true) {m_bWritable = value;}
    bool isReadable() const             {return m_bReadable;}
    void setReadable(bool value = true) {m_bReadable = value;}

protected:
    virtual bool canSetValue(TYPEVAR varType) const;
    virtual bool canGetValue() const;

private:
    E1Property(const E1ComponentObject &component, long lIndex, const QString &qsName);

    bool m_bWritable;
    bool m_bReadable;
};

#endif // E1PROPERTY_H
