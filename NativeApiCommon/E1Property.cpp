#include "E1ComponentObject.h"
#include "E1Property.h"

///----------------------------------------------------------------------------
E1Property::E1Property(const E1ComponentObject &component, long lIndex, const QString &qsName)
    : E1Value(component, lIndex, qsName),
      m_bWritable(true),
      m_bReadable(true)
{
}

///////////////////////////////////////////////////////////////////////////////
/// Protected
///----------------------------------------------------------------------------
bool E1Property::canSetValue(TYPEVAR varType) const
{
    return m_bWritable && E1Value::canSetValue(varType);
}

///----------------------------------------------------------------------------
bool E1Property::canGetValue() const
{
    return m_bReadable && E1Value::canGetValue();
}
