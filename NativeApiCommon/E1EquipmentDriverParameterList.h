#ifndef E1EQUIPMENTDRIVERPARAMETERLIST_H
#define E1EQUIPMENTDRIVERPARAMETERLIST_H

#include "E1InterfaceItemList.h"
#include "E1EquipmentDriverParameter.h"

class E1EquipmentDriverParameterList
        : public E1InterfaceItemList<E1EquipmentDriverParameter>
{
public:
    E1EquipmentDriverParameterList(const E1ComponentObject &component);
    virtual ~E1EquipmentDriverParameterList() {}

    virtual E1EquipmentDriverParameter& append(E1Property& property, const QString qsCaption);

    QString xml(int indent = 1);
};

#endif // E1EQUIPMENTDRIVERPARAMETERLIST_H
