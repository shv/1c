#ifndef E1PROPERTYLIST_H
#define E1PROPERTYLIST_H

#include "E1InterfaceItemList.h"
#include "E1Property.h"

class E1PropertyList
        : public E1InterfaceItemList<E1Property>
{
public:
    E1PropertyList(const E1ComponentObject &component) : E1InterfaceItemList(component) {}
    virtual ~E1PropertyList() {}

    bool isWritable(long lIndex) const;
    bool isReadable(long lIndex) const;

    bool setValue(long lIndex, const tVariant* pValue);
    bool getValue(long lIndex, tVariant* pValue) const;
};

#endif // E1PROPERTYLIST_H
