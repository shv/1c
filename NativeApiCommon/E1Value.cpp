#include <QDateTime>

#include "E1ComponentObject.h"
#include "E1Value.h"
#include "E1cConnection.h"

///----------------------------------------------------------------------------
E1Value::E1Value(const E1Value &value)
    : E1InterfaceItem(value)
{
    tVarInit(&m_vValue);

    tVariant* pvProperty = new tVariant;
    value.getValue(pvProperty);
    setValue(pvProperty);
    delete pvProperty;
}

E1Value::E1Value(const E1ComponentObject& component, const QString &qsName)
    : E1InterfaceItem(component, 0, qsName)
{
    tVarInit(&m_vValue);
}

///----------------------------------------------------------------------------
E1Value::E1Value(const E1ComponentObject &component, long lIndex, const QString &qsName)
    : E1InterfaceItem(component, lIndex, qsName)
{
    tVarInit(&m_vValue);
}

E1Value::~E1Value()
{
    clear();
}

///----------------------------------------------------------------------------
bool E1Value::setValue(const tVariant* pValue, bool bForce)
{
    if (pValue == NULL || !(bForce || canSetValue(TV_VT(pValue)))) return false;
//    qWarning() << "E1Value::setValue(tVariant): pValue->vt =" << pValue->vt;

    bool bSuccess = true;

    switch (pValue->vt) {
    case VTYPE_EMPTY:
    case VTYPE_I2:
    case VTYPE_I4:
    case VTYPE_I8:
    case VTYPE_ERROR:
    case VTYPE_UI1:
    case VTYPE_BOOL:
    case VTYPE_R4:
    case VTYPE_R8:
    case VTYPE_DATE:
    case VTYPE_TM:
        {
            clear();
            ::memcpy((void*)&m_vValue, (void*)pValue, sizeof(tVariant));
        }
        break;
    case VTYPE_PSTR:
    case VTYPE_BLOB:
        {
            clear();
            ::memcpy((void*)&m_vValue, (void*)pValue, sizeof(tVariant));
            int len = m_vValue.strLen + (pValue->vt == VTYPE_PSTR) ? 1 : 0;
            m_vValue.pstrVal = new char[len];
            ::memcpy((void*)m_vValue.pstrVal, (void*)pValue->pstrVal, len * sizeof(char));
        }
    break;
    case VTYPE_PWSTR:
        {
            clear();
            ::memcpy((void*)&m_vValue, (void*)pValue, sizeof(tVariant));
            int len = m_vValue.wstrLen + 1;
            m_vValue.pwstrVal = new WCHAR_T[len];
            ::memcpy((void*)m_vValue.pwstrVal, (void*)pValue->pwstrVal, len * sizeof(WCHAR_T));
        }
        break;
    default:
        bSuccess = false;
    }
//    if (bSuccess) qWarning() << "E1Value::setValue(tVariant):" << this->toString();
//    else          qWarning() << "E1Value::setValue(tVariant): FAILED";

    return bSuccess;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(int value, bool bForce)
{
    qWarning() << "E1Value::setValue(int):";
    if (!(bForce || canSetValue(VTYPE_I4))) return false;

    clear();
    DATA_SET(&m_vValue, VTYPE_I4, lVal, value)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(long value, bool bForce)
{
    qWarning() << "E1Value::setValue(long):";
    if (!(bForce || canSetValue(VTYPE_I4))) return false;

    clear();
    DATA_SET(&m_vValue, VTYPE_I4, lVal, value)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(qlonglong value, bool bForce)
{
    qWarning() << "E1Value::setValue(qlonglong):" << canSetValue(VTYPE_I8);
    if (!(bForce || canSetValue(VTYPE_I8))) return false;

    clear();
    DATA_SET(&m_vValue, VTYPE_I8, lVal, value)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(double value, bool bForce)
{
    qWarning() << "E1Value::setValue(double):";
    if (!(bForce || canSetValue(VTYPE_R8))) return false;

    clear();
    DATA_SET(&m_vValue, VTYPE_R8, dblVal, value)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(bool value, bool bForce)
{
//    qWarning() << "E1Value::setValue(bool):";
    if (!(bForce || canSetValue(VTYPE_BOOL))) return false;

    clear();
    DATA_SET(&m_vValue, VTYPE_BOOL, bVal, value)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(int year, int month, int day, int hour, int min, int sec, bool bForce)
{
//    qDebug() << !canSetValue(VTYPE_TM);
    if (!(bForce || canSetValue(VTYPE_TM))) return false;

    tm date;
    date.tm_year = year - 1900;
    date.tm_mon = month - 1;
    date.tm_mday = day;
    date.tm_hour = hour;
    date.tm_min = min;
    date.tm_sec = sec;

    clear();
    DATA_SET(&m_vValue, VTYPE_TM, tmVal, date)                       \

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(const char* value, bool bForce)
{
    return setValue(QString(value), bForce);
}

///----------------------------------------------------------------------------
bool E1Value::setValue(const QByteArray &value, bool bForce)
{
//    qWarning() << "E1Value::setValue(QByteArray):";
    if (!(bForce || canSetValue(VTYPE_BLOB))) return false;

    clear();
    int length = value.size() - 1;
    char* str = new char[length];
    ::memcpy((void*)str, (void*)value.constData(), length);

    DATA_SET(&m_vValue, VTYPE_BLOB, pstrVal, str);
    m_vValue.strLen = length;

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(const QString &value, bool bForce)
{
//    qWarning() << "E1Value::setValue(QString):";
    if (!(bForce || canSetValue(VTYPE_PWSTR))) return false;

    clear();
    size_t length = value.length() + 1;
    WCHAR_T* wStr = new WCHAR_T[length];
    ::memcpy((void*)wStr, (void*)value.utf16(), length * sizeof(WCHAR_T));

    DATA_SET(&m_vValue, VTYPE_PWSTR, pwstrVal, wStr);
    m_vValue.wstrLen = value.length();

    return true;
}

///----------------------------------------------------------------------------
bool E1Value::setValue(const E1Value& value, bool bForce)
{
//    qWarning() << "E1Value::setValue(E1Value):";
    if (this == &value) return true;

    tVariant* pvValue = new tVariant;
    tVarInit(pvValue);

    value.getValue(pvValue, true);
    bool bResult = setValue(pvValue, bForce);

    delete pvValue;

    return bResult;
}

///----------------------------------------------------------------------------
bool E1Value::getValue(tVariant* pValue, bool bForce) const
{
    clear(m_Component, pValue);
    if (!(bForce || canGetValue()) || pValue == NULL) return false;

    switch (m_vValue.vt) {
    case VTYPE_EMPTY:
    case VTYPE_I2:
    case VTYPE_I4:
    case VTYPE_I8:
    case VTYPE_ERROR:
    case VTYPE_UI1:
    case VTYPE_BOOL:
    case VTYPE_R4:
    case VTYPE_R8:
    case VTYPE_DATE:
    case VTYPE_TM:
        ::memcpy((void*)pValue, (void*)&m_vValue, sizeof(tVariant));
        break;
    case VTYPE_PSTR:
    case VTYPE_BLOB:
        {
            char* str = NULL;
            size_t bytes = (m_vValue.strLen + 1) * sizeof(char);
            if (!m_Component.allocateWith1C((void**)&str, bytes)) return false;

            ::memcpy((void*)pValue, (void*)&m_vValue, sizeof(tVariant));
            ::memcpy((void*)str, (void*)m_vValue.pstrVal, bytes);
            pValue->pstrVal = str;
        }
        break;
    case VTYPE_PWSTR:
        {
            WCHAR_T* str = NULL;
            size_t bytes = (m_vValue.strLen + 1) * sizeof(WCHAR_T);
            if (!m_Component.allocateWith1C((void**)&str, bytes)) return false;

            ::memcpy((void*)pValue, (void*)&m_vValue, sizeof(tVariant));
            ::memcpy((void*)str, (void*)m_vValue.pwstrVal, bytes);
            pValue->pwstrVal = str;
        }
        break;
    }
    return true;
}

///----------------------------------------------------------------------------
bool E1Value::toBool() const
{
    bool result = false;

    switch (m_vValue.vt) {
    case VTYPE_BOOL:
        result = m_vValue.bVal;
        break;
    case VTYPE_I2:
    case VTYPE_I4:
    case VTYPE_I8:
    case VTYPE_ERROR:
    case VTYPE_UI1:
        result = (bool) m_vValue.lVal;
        break;
    case VTYPE_R4:
    case VTYPE_R8:
        result = (bool) m_vValue.dblVal;
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
int E1Value::toInt() const
{
//    VTYPE_EMPTY    = 0,
//    VTYPE_NULL,

//    VTYPE_INT,        int             intVal
//    VTYPE_I1,         int8_t          i8Val
//    VTYPE_I2,         int16_t         shortVal
//    VTYPE_I4,         int32_t         lVal
//    VTYPE_I8,         int64_t         llVal

//    VTYPE_UINT,       unsigned int    uintVal
//    VTYPE_UI1,        uint8_t         ui8Val
//    VTYPE_UI2,        uint16_t        ushortVal
//    VTYPE_UI4,        uint32_t        ulVal
//    VTYPE_UI8,        uint64_t        ullVal

//    VTYPE_R4,         float           fltVal
//    VTYPE_R8,         double          dblVal

//    VTYPE_BOOL,       bool            bVal

//    VTYPE_ERROR,      int32_t         errCode

//    VTYPE_PSTR,       struct str      string
//    VTYPE_DATE,       DATE (double)
//    VTYPE_TM,         struct          tm
//    VTYPE_INTERFACE,  struct          iface
//    VTYPE_VARIANT,    struct          _tVariant*

    int result = 0;

    switch (m_vValue.vt) {
    case VTYPE_INT:
        result = m_vValue.intVal;
        break;
    case VTYPE_I1:
        result = m_vValue.i8Val;
        break;
    case VTYPE_I2:
        result = m_vValue.shortVal;
        break;
    case VTYPE_I4:
        result = m_vValue.lVal;
        break;
    case VTYPE_I8:
        result = m_vValue.llVal;
        break;
    case VTYPE_UINT:
        result = m_vValue.uintVal;
        break;
    case VTYPE_UI1:
        result = m_vValue.ui8Val;
        break;
    case VTYPE_UI2:
        result = m_vValue.ushortVal;
        break;
    case VTYPE_UI4:
        result = m_vValue.ulVal;
        break;
    case VTYPE_UI8:
        result = m_vValue.ullVal;
        break;
    case VTYPE_R4:
        result = m_vValue.fltVal;
        break;
    case VTYPE_R8:
        result = m_vValue.dblVal;
        break;
    case VTYPE_BOOL:
        result = m_vValue.bVal;
        break;
    case VTYPE_ERROR:
        result = m_vValue.errCode;
        break;
    case VTYPE_PSTR:
        result = QString(m_vValue.pstrVal).toInt();
        break;
    case VTYPE_PWSTR:
        result = QString::fromUtf16((ushort*)m_vValue.pwstrVal).toInt();
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
long E1Value::toLong() const
{
    long result = 0;

    switch (m_vValue.vt) {
    case VTYPE_INT:
        result = m_vValue.intVal;
        break;
    case VTYPE_I1:
        result = m_vValue.i8Val;
        break;
    case VTYPE_I2:
        result = m_vValue.shortVal;
        break;
    case VTYPE_I4:
        result = m_vValue.lVal;
        break;
    case VTYPE_I8:
        result = m_vValue.llVal;
        break;
    case VTYPE_UINT:
        result = m_vValue.uintVal;
        break;
    case VTYPE_UI1:
        result = m_vValue.ui8Val;
        break;
    case VTYPE_UI2:
        result = m_vValue.ushortVal;
        break;
    case VTYPE_UI4:
        result = m_vValue.ulVal;
        break;
    case VTYPE_UI8:
        result = m_vValue.ullVal;
        break;
    case VTYPE_R4:
        result = m_vValue.fltVal;
        break;
    case VTYPE_R8:
        result = m_vValue.dblVal;
        break;
    case VTYPE_BOOL:
        result = m_vValue.bVal;
        break;
    case VTYPE_ERROR:
        result = m_vValue.errCode;
        break;
    case VTYPE_PSTR:
        result = QString(m_vValue.pstrVal).toLong();
        break;
    case VTYPE_PWSTR:
        result = QString::fromUtf16((ushort*)m_vValue.pwstrVal).toLong();
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
qlonglong E1Value::toLongLong() const
{
    qlonglong result = 0;

    switch (m_vValue.vt) {
    case VTYPE_INT:
        result = m_vValue.intVal;
        break;
    case VTYPE_I1:
        result = m_vValue.i8Val;
        break;
    case VTYPE_I2:
        result = m_vValue.shortVal;
        break;
    case VTYPE_I4:
        result = m_vValue.lVal;
        break;
    case VTYPE_I8:
        result = m_vValue.llVal;
        break;
    case VTYPE_UINT:
        result = m_vValue.uintVal;
        break;
    case VTYPE_UI1:
        result = m_vValue.ui8Val;
        break;
    case VTYPE_UI2:
        result = m_vValue.ushortVal;
        break;
    case VTYPE_UI4:
        result = m_vValue.ulVal;
        break;
    case VTYPE_UI8:
        result = m_vValue.ullVal;
        break;
    case VTYPE_R4:
        result = m_vValue.fltVal;
        break;
    case VTYPE_R8:
        result = m_vValue.dblVal;
        break;
    case VTYPE_BOOL:
        result = m_vValue.bVal;
        break;
    case VTYPE_ERROR:
        result = m_vValue.errCode;
        break;
    case VTYPE_PSTR:
        result = QString(m_vValue.pstrVal).toLongLong();
        break;
    case VTYPE_PWSTR:
        result = QString::fromUtf16((ushort*)m_vValue.pwstrVal).toLongLong();
        break;
    }
    return result;

}

///----------------------------------------------------------------------------
double E1Value::toDouble() const
{
    double result = 0.0;

    switch (m_vValue.vt) {
    case VTYPE_I2:
        result = m_vValue.shortVal;
        break;
    case VTYPE_I4:
        result = m_vValue.lVal;
        break;
    case VTYPE_I8:
        result = m_vValue.llVal;
        break;
    case VTYPE_ERROR:
        result = m_vValue.errCode;
        break;
    case VTYPE_UI1:
        result = m_vValue.ui8Val;
        break;
    case VTYPE_BOOL:
        result = m_vValue.bVal;
        break;
    case VTYPE_R4:
        result = m_vValue.fltVal;
        break;
    case VTYPE_R8:
        result = m_vValue.dblVal;
        break;
    case VTYPE_PSTR:
        result = QString(m_vValue.pstrVal).toDouble();
        break;
    case VTYPE_PWSTR:
        result = QString::fromUtf16((ushort*)m_vValue.pwstrVal).toDouble();
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
QString E1Value::toString() const
{
    QString result("");

    switch (m_vValue.vt) {
    case VTYPE_I2:
        result = QString("%1").arg(m_vValue.shortVal);
        break;
    case VTYPE_I4:
        result = QString("%1").arg(m_vValue.lVal);
        break;
    case VTYPE_I8:
        result = QString("%1").arg(m_vValue.llVal);
        break;
    case VTYPE_ERROR:
        result = QString("%1").arg(m_vValue.errCode);
        break;
    case VTYPE_UI1:
        result = QString("%1").arg(m_vValue.ui8Val);
        break;
    case VTYPE_BOOL:
        result = m_vValue.bVal ? "true" : "false";
        break;
    case VTYPE_R4:
        result = QString("%1").arg(m_vValue.fltVal);
        break;
    case VTYPE_R8:
        result = QString("%1").arg(m_vValue.dblVal);
        break;
    case VTYPE_TM: {
            tm* tmTmp = new tm;
            ::memcpy(tmTmp, &m_vValue.tmVal, sizeof(tm));
            QDateTime dt;
            dt.setTime_t(mktime(tmTmp));
            delete tmTmp;
            result = dt.toString(Qt::SystemLocaleShortDate);
        }
        break;
    case VTYPE_PSTR:
        result = QString(m_vValue.pstrVal);
        break;
    case VTYPE_PWSTR:
        result = QString::fromUtf16((ushort*)m_vValue.pwstrVal);
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
QDateTime E1Value::toDateTime() const
{
    QDateTime result;

    switch (m_vValue.vt) {
    case VTYPE_TM: {
            tm* tmTmp = new tm;
            ::memcpy(tmTmp, &m_vValue.tmVal, sizeof(tm));
            result.setTime_t(mktime(tmTmp));
            delete tmTmp;
        }
        break;
    }
    return result;
}

///----------------------------------------------------------------------------
E1Value& E1Value::operator=(const E1Value& right)
{
    if (this == &right) return *this;
    setValue(right);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
/// Protected
///----------------------------------------------------------------------------
bool E1Value::canSetValue(TYPEVAR varType) const
{
    return isEmpty() || m_vValue.vt == varType;
}

///----------------------------------------------------------------------------
bool E1Value::canGetValue() const
{
    return true;
}

///////////////////////////////////////////////////////////////////////////////
/// Utilities
///----------------------------------------------------------------------------
void E1Value::clear()
{
    tVariant* pValue = &m_vValue;
    TYPEVAR varType = pValue->vt;

    switch (varType) {
    case VTYPE_PSTR:
    case VTYPE_BLOB:
        delete[] pValue->pstrVal;
        break;
    case VTYPE_PWSTR:
        delete[] pValue->pwstrVal;
        break;
    }

    tVarInit(pValue);
    pValue->vt = varType;
}

///----------------------------------------------------------------------------
void E1Value::clear(const E1ComponentObject& /*component*/, tVariant* pValue)
{
    if (pValue == NULL) return;

    TYPEVAR varType = pValue->vt;

    switch (varType) {
    case VTYPE_PSTR:
    case VTYPE_BLOB:
        assert(component.freeWith1C((void**)&(pValue->pstrVal)));
        break;
    case VTYPE_PWSTR:
        assert(component.freeWith1C((void**)&(pValue->pwstrVal)));
        break;
    }

    tVarInit(pValue);
    pValue->vt = varType;
}

///----------------------------------------------------------------------------
void E1Value::clear(const E1cConnection &/*connection*/, tVariant* pValue)
{
    if (pValue == NULL) return;

    TYPEVAR varType = pValue->vt;

    switch (varType) {
    case VTYPE_PSTR:
    case VTYPE_BLOB:
        assert(connection.freeWith1C((void**)&(pValue->pstrVal)));
        break;
    case VTYPE_PWSTR:
        assert(connection.freeWith1C((void**)&(pValue->pwstrVal)));
        break;
    }

    tVarInit(pValue);
    pValue->vt = varType;
}

