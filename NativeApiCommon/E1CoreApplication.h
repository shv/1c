#ifndef E1COREAPPLICATION_H
#define E1COREAPPLICATION_H

#include <QCoreApplication>
#include <QThread>
#include <QMutex>
#include <QMap>

#include "E1ComponentObject.h"

class E1CoreApplication;
typedef E1CoreApplication E1App;

class E1CoreApplication
{
public:
    static E1CoreApplication& inst();
    static void               free();

    void startThread(QObject* pObj);
    void stopThread(QObject* pObj);

private:
    E1CoreApplication();
    virtual ~E1CoreApplication();

    static E1CoreApplication* m_pSelf;

    QMutex            m_Mutex;
    QCoreApplication* m_pCoreApp;
    bool              m_bDeleteApp;

    QMap<QObject*, QThread*>  m_Threads;
};

#endif // E1COREAPPLICATION_H
