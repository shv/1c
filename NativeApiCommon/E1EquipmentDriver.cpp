#include "E1EquipmentDriver.h"

#define ADD_FUNCTION(_member, _name_en) ADD_E1_FUNCTION(E1EquipmentDriver, _member, _name_en)

///----------------------------------------------------------------------------
E1EquipmentDriver::E1EquipmentDriver(const QString &qsComName, long lVersion, int iType)
    : E1ComponentObject(qsComName, lVersion),
      m_Parameters(*this),
      m_iType(iType),
      m_qsUrl(""),
      m_bDemoMode(false),
      m_Actions(*this)
{
    qWarning() << "E1EquipmentDriver::E1EquipmentDriver():";
    // Define properties

    // Define methods
    ADD_FUNCTION( getVersionString, "GetVersion");
    RUS_FUNCTION( getVersionString, "ПолучитьНомерВерсии");

    ADD_FUNCTION( getDescription, "GetDescription");
    RUS_FUNCTION( getDescription, "ПолучитьОписание");
    ADD_PARAMETER(getDescription, "Name", qsComName);
    ADD_PARAMETER(getDescription, "Description", "");
    ADD_PARAMETER(getDescription, "EquipmentType", "");
    ADD_PARAMETER(getDescription, "InterfaceRevision", 1004);
    ADD_PARAMETER(getDescription, "IntegrationLibrary", false);
    ADD_PARAMETER(getDescription, "MainDriverInstalled", true);
    ADD_PARAMETER(getDescription, "DownloadURL", "");

    ADD_FUNCTION( getLastError, "GetLastError");
    RUS_FUNCTION( getLastError, "ПолучитьОшибку");
    ADD_PARAMETER(getLastError, "ErrorDescription", "");

    ADD_FUNCTION( getParameters, "GetParameters");
    RUS_FUNCTION( getParameters, "ПолучитьПараметры");
    ADD_PARAMETER(getParameters, "TableParameters", "");

    ADD_FUNCTION( setParameter, "SetParameter");
    RUS_FUNCTION( setParameter, "УстановитьПараметр");
    ADD_PARAMETER(setParameter, "Name", "");
    ADD_PARAMETER(setParameter, "Value", (tVariant*)NULL);

    ADD_FUNCTION( open, "Open");
    RUS_FUNCTION( open, "Подключить");
    ADD_PARAMETER(open, "DeviceID", "");

    ADD_FUNCTION( close, "Close");
    RUS_FUNCTION( close, "Отключить");
    ADD_PARAMETER(close, "DeviceID", "");

    ADD_FUNCTION( deviceTest, "DeviceTest");
    RUS_FUNCTION( deviceTest, "ТестУстройства");
    ADD_PARAMETER(deviceTest, "Description", "");
    ADD_PARAMETER(deviceTest, "DemoModeIsActivated", "");

    ADD_FUNCTION( getAdditionalActions, "GetAdditionalActions");
    RUS_FUNCTION( getAdditionalActions, "ПолучитьДополнительныеДействия");
    ADD_PARAMETER(getAdditionalActions, "TableActions", "");

    ADD_FUNCTION( doAdditionalAction, "DoAdditionalAction");
    RUS_FUNCTION( doAdditionalAction, "ВыполнитьДополнительноеДействие");
    ADD_PARAMETER(doAdditionalAction, "ActionName", "");
}

///----------------------------------------------------------------------------
QString E1EquipmentDriver::getVersionString() const
{
    long lVersion = getVersion();
    return QString("%1.%2").arg(int(lVersion / 1000)).arg(int(lVersion % 1000), 3, 10, QLatin1Char('0'));
}

///----------------------------------------------------------------------------
QString E1EquipmentDriver::getEquipmentType(int iType)
{
    QString qsResult = "";

//    switch(E1Tr::inst().getLang()) {
//    case 0: break;
//    case 1:

        switch(iType) {
            case BarcodeScanner:         qsResult = "СканерШтрихкода";          break;
            case CardReader:             qsResult = "СчитывательМагнитныхКарт"; break;
            case FiscalRegister:         qsResult = "ФискальныйРегистратор";    break;
            case ReceiptPrinter:         qsResult = "ПринтерЧеков";             break;
            case LabelPrinter:           qsResult = "ПринтерЭтикеток";          break;
            case CustomerDisplay:        qsResult = "ДисплейПокупателя";        break;
            case DataCollectionTerminal: qsResult = "ТерминалСбораДанных";      break;
            case AcquiringTerminal:      qsResult = "ЭквайринговыйТерминал";    break;
            case ElectronicScales:       qsResult = "ЭлектронныеВесы";          break;
            case LabelPrintingScales:    qsResult = "ВесыСПечатьюЭтикеток";     break;
            case CashRegisterOffline:    qsResult = "ККМOffline";               break;
        }
//        break;
//    }

    return qsResult;
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::openConnection(QString& /*qsDeviceID*/)
{
    // Reimplement in descendant
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::closeConnection(const QString& /*qsDeviceID*/)
{
    // Reimplement in descendant
}

///----------------------------------------------------------------------------
bool E1EquipmentDriver::doAdditionalAction(const QString& qsActionName)
{
    bool bResult = true;
    QString qsData("");

    try {
        E1EquipmentDriverAction& action = m_Actions[qsActionName];
        bResult = action.doAction(qsData);
        if (action.firesExternalEvent()) m_1cConnection.externalEvent(m_qsComponentName, action.getName(), qsData);
    }
    catch(E1Exception& e) {
        if (e.getCode() != E1Exception::NameNotFound) {
            setLastError(e);
            qWarning() << e.getDescription();
            bResult = false;
        }
    }

    return bResult;
}

///////////////////////////////////////////////////////////////////////////////
/// Interface functions
///----------------------------------------------------------------------------
void E1EquipmentDriver::getVersionString(const E1Method& /*method*/, E1ValueList& /*parameters*/, E1Value& returnValue)
{
    returnValue.setValue(getVersionString());
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::getDescription(const E1Method &method, E1ValueList &parameters, E1Value &returnValue)
{
    parameters["Name"               ].setValue(method.getDefaultValue("Name"));
    parameters["Description"        ].setValue(getDescription());
    parameters["EquipmentType"      ].setValue(getEquipmentType(m_iType));
    parameters["InterfaceRevision"  ].setValue(method.getDefaultValue("InterfaceRevision"));
    parameters["IntegrationLibrary" ].setValue(method.getDefaultValue("IntegrationLibrary"));
    parameters["MainDriverInstalled"].setValue(method.getDefaultValue("MainDriverInstalled"));
    parameters["DownloadURL"        ].setValue(getDownloadURL());

    returnValue.setValue(true);
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::getLastError(const E1Method& /*method*/, E1ValueList &parameters, E1Value &returnValue)
{
    parameters["ErrorDescription"].setValue(m_LastError.getDescription());

    returnValue.setValue(m_LastError.getCode());
    qDebug() << "E1EquipmentDriver::getLastError" << m_LastError.getCode() << m_LastError.getDescription();

    m_LastError.clear();
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::getParameters(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    parameters["TableParameters"].setValue(m_Parameters.xml());

    returnValue.setValue(true);
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::setParameter(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    bool bResult = setProperty(parameters["Name"], parameters["Value"]);

    returnValue.setValue(bResult);
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::open(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    qDebug() << "E1EquipmentDriver::open";
    QString qsDeviceID = parameters["DeviceID"];

    try {
        openConnection(qsDeviceID);

        parameters["DeviceID"].setValue(qsDeviceID);
        returnValue.setValue(true);
    }
    catch(E1Exception& e) {
        setLastError(e);

        returnValue.setValue(false);
        qDebug() << "E1EquipmentDriver::open ERROR" << e.getDescription();
    }
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::close(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    qDebug() << "E1EquipmentDriver::close";
    try {
        closeConnection(QString(parameters["DeviceID"]));
        parameters["DeviceID"].setValue("");
        returnValue.setValue(true);
    }
    catch(E1Exception& e) {
        setLastError(e);

        returnValue.setValue(false);
        qDebug() << "E1EquipmentDriver::close ERROR" << e.getDescription();
    }
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::deviceTest(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    qDebug() << "E1EquipmentDriver::deviceTest";
    QString qsDeviceID = "";
    QString qsResultDescription = "";

    try {
        openConnection(qsDeviceID);
        qsResultDescription = testDevice(qsDeviceID);
        closeConnection(qsDeviceID);

        returnValue.setValue(true);
    }
    catch(E1Exception& e) {
        qDebug() << "E1EquipmentDriver::deviceTest ERROR" << e.getDescription();
        setLastError(e);
        qsResultDescription = e.getDescription();

        returnValue.setValue(false);
    }

    parameters["Description"        ].setValue(qsResultDescription);
    parameters["DemoModeIsActivated"].setValue(isDemoMode() ? getDemoModeDescription() : "");
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::getAdditionalActions(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    parameters["TableActions"].setValue(m_Actions.xml());

    returnValue.setValue(true);
}

///----------------------------------------------------------------------------
void E1EquipmentDriver::doAdditionalAction(const E1Method& /*method*/, E1ValueList& parameters, E1Value &returnValue)
{
    returnValue.setValue(doAdditionalAction(parameters["ActionName"]));
}
