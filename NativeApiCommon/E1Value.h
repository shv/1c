#ifndef E1VALUE_H
#define E1VALUE_H

#include "E1InterfaceItem.h"

#include "types.h"

class E1ComponentObject;
class E1cConnection;

class E1Value
        : public E1InterfaceItem
{
public:
    E1Value(const E1Value &value);
    E1Value(const E1ComponentObject &component, const QString &qsName);
    E1Value(const E1ComponentObject &component, long lIndex, const QString &qsName);

    virtual ~E1Value();

    bool isEmpty() const {return m_vValue.vt == VTYPE_EMPTY;}

    bool setValue(const tVariant* pValue, bool bForce = false);
    bool setValue(int value, bool bForce = false);
    bool setValue(long value, bool bForce = false);
    bool setValue(qlonglong value, bool bForce = false);
    bool setValue(double value, bool bForce = false);
    bool setValue(bool value, bool bForce = false);
    bool setValue(int year, int month, int day, int hour = 0, int min = 0, int sec = 0, bool bForce = false);
    bool setValue(const char* value, bool bForce = false);
    bool setValue(const QByteArray &value, bool bForce = false);
    bool setValue(const QString &value, bool bForce = false);
    bool setValue(const E1Value &value, bool bForce = false);

    bool getValue(tVariant* pValue, bool bForce = false) const;

    bool      toBool()     const;
    int       toInt()      const;
    long      toLong()     const;
    qlonglong toLongLong() const;
    double    toDouble()   const;
    QString   toString()   const;
    QDateTime toDateTime() const;

    E1Value& operator=(const E1Value& right);
    operator bool()      const {return toBool();}
    operator int()       const {return toInt();}
    operator long()      const {return toLong();}
    operator qlonglong() const {return toLongLong();}
    operator double()    const {return toDouble();}
    operator QString()   const {return toString();}

    void clear();
    static void clear(const E1ComponentObject& component, tVariant* pValue);
    static void clear(const E1cConnection& connection, tVariant* pValue);

protected:
    virtual bool canSetValue(TYPEVAR varType) const;
    virtual bool canGetValue() const;

private:
    tVariant m_vValue;
};

#endif // E1VALUE_H
