#include <QDomDocument>
#include <assert.h>
#include "E1Property.h"
#include "E1EquipmentDriverParameter.h"

const QChar g_qcSep(QChar::LineFeed);

///----------------------------------------------------------------------------
E1EquipmentDriverParameter::E1EquipmentDriverParameter(const E1ComponentObject &component, long lIndex, const QString &qsName)
    : E1InterfaceItem(component, lIndex, qsName),
      m_pProperty(NULL),
      m_pMasterProperty(NULL)
{
}

///----------------------------------------------------------------------------
void E1EquipmentDriverParameter::setAttributes(const E1EquipmentDriverParameter::Attributes& attributes, int iAlias)
{
    if (!hasAttributes() && iAlias != E1Tr::English) m_Attributes.insert(E1Tr::English, attributes);

    m_Attributes.insert(iAlias, attributes);
}

///----------------------------------------------------------------------------
E1EquipmentDriverParameter::Attributes E1EquipmentDriverParameter::getAttributes() const
{
    E1EquipmentDriverParameter::Attributes empty;
    if (!hasAttributes()) return empty;

    QMap<int, E1EquipmentDriverParameter::Attributes>::const_iterator it = m_Attributes.constFind(E1Tr::inst().getLang());
    return (it == m_Attributes.constEnd()) ? m_Attributes.constFind(E1Tr::English).value() : it.value();
}

///----------------------------------------------------------------------------
E1EquipmentDriverParameter::Attributes E1EquipmentDriverParameter::getAttributesEng() const
{
    E1EquipmentDriverParameter::Attributes empty;
    if (!hasAttributes()) return empty;

    return  m_Attributes.constFind(E1Tr::English).value();
}

///----------------------------------------------------------------------------
QString E1EquipmentDriverParameter::getTypeValue() const
{
    if (m_pProperty == NULL) return "";

    QString qsResult;

    tVariant* pvProperty = new tVariant;
    m_pProperty->getValue(pvProperty);
    switch (pvProperty->vt) {
    case VTYPE_BOOL:
        qsResult = "Boolean";
        break;
    case VTYPE_I2:
    case VTYPE_I4:
    case VTYPE_I8:
    case VTYPE_ERROR:
    case VTYPE_UI1:
    case VTYPE_R4:
    case VTYPE_R8:
        qsResult = "Number";
        break;
    case VTYPE_TM:
        qsResult = "DateTime";
        break;
    default:
        qsResult = "String";
    }

    delete pvProperty;

    return qsResult;
}

///----------------------------------------------------------------------------
void E1EquipmentDriverParameter::makeDomChildFor(QDomElement &parent)
{
    QDomDocument doc = parent.ownerDocument();

    QDomElement parameter = doc.createElement("Parameter");
    parameter.setAttribute("Name", getName());
    parameter.setAttribute("Caption", getCaption());
    parameter.setAttribute("TypeValue", getTypeValue());
    parameter.setAttribute("DefaultValue", m_pProperty->toString());

    if (!m_pProperty->isWritable()) parameter.setAttribute("ReadOnly", "true");

    if (hasAttributes()) {
        E1EquipmentDriverParameter::Attributes att = getAttributes();

        if (!att.Description.isEmpty()) parameter.setAttribute("Description", att.Description);

        if (!att.ChoiceList.isEmpty()) {
            QDomElement choiceList = doc.createElement("ChoiceList");

            QMap<QString, QString>::const_iterator i = att.ChoiceList.constBegin();
            while (i != att.ChoiceList.constEnd()) {
                QDomElement choiceItem = doc.createElement("Item");
                choiceItem.setAttribute("Value", i.key());

                QDomText choiceValue = doc.createTextNode(i.value());
                choiceItem.appendChild(choiceValue);

                choiceList.appendChild(choiceItem);

                ++i;
            }

            parameter.appendChild(choiceList);
        }

        if (!att.FieldFormat.isEmpty()) parameter.setAttribute("FieldFormat", att.FieldFormat);
    }

    parent.appendChild(parameter);
}
