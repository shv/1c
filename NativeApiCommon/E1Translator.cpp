#include <QDebug>
#include "E1Translator.h"

E1Translator* E1Translator::m_pSelf = NULL;

///----------------------------------------------------------------------------
E1Translator& E1Translator::inst()
{
    if(!m_pSelf) m_pSelf = new E1Translator();
    return *m_pSelf;
}

///----------------------------------------------------------------------------
void E1Translator::free()
{
    if (m_pSelf) {
        delete m_pSelf;
        m_pSelf = NULL;
    }
}

///----------------------------------------------------------------------------
QString E1Translator::loc(const QMap<int, QString>& map) const
{
    QMap<int, QString>::const_iterator it = map.constFind(m_iLangAlias);
    return (it == map.constEnd()) ? map.constFind(English).value() : it.value();
}

///----------------------------------------------------------------------------
QString E1Translator::eng(const QMap<int, QString> &map) const
{
    return map.constFind(English).value();
}
