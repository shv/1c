#ifndef E1VALUELIST_H
#define E1VALUELIST_H

#include "E1InterfaceItemList.h"
#include "E1Value.h"

class E1ComponentObject;

class E1ValueList
        : public E1InterfaceItemList<E1Value>
{
public:
    E1ValueList(const E1ComponentObject &component) : E1InterfaceItemList(component) {}//qWarning();}
    virtual ~E1ValueList() {}//qWarning();}

    E1Value& append(const QString &qsName);

    bool setValue(long lIndex, const tVariant* pValue);
    bool getValue(long lIndex, tVariant* pValue) const;
};

#endif // E1VALUELIST_H
