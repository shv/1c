#ifndef E1TRANSLATOR_H
#define E1TRANSLATOR_H

#include <QMap>
#include <QString>

class E1Translator;
typedef E1Translator E1Tr;

class E1Translator
{
public:
    static E1Translator& inst();
    static void          free();

    enum Locale {
        English = 0,
        Russian = 1
    };

//    static const int English = 0;
//    static const int Russian = 1;

    inline void setLang(int iAlias) {m_iLangAlias = iAlias;}
    inline int  getLang() const     {return m_iLangAlias;}

    QString loc(const QMap<int, QString>& map) const;
    QString eng(const QMap<int, QString>& map) const;

private:
    E1Translator() : m_iLangAlias(English) {}
    ~E1Translator() {}

    static E1Translator* m_pSelf;

    int m_iLangAlias;
};

#define TR_INIT(text)                       \
    QMap<int, QString> map;                 \
    map.insert(E1Tr::English, text)

#define TR_ENG(text)                        \
    map.insert(E1Tr::English, text)

#define TR_RUS(text)                        \
    map.insert(E1Tr::Russian, text)

#define TR                                  \
    E1Tr::inst().loc(map)

#endif // E1TRANSLATOR_H
