#ifndef E1COMPONENT_H
#define E1COMPONENT_H

#include <QMap>
#include <QVector>
#include <QString>

#include "E1ComponentObject.h"
#include "E1ComponentObjectFactory.h"

class E1Component;
typedef E1Component E1Cmp;

class E1Component
{
public:
    static E1Component& inst();
    static void         free();

    const QString&     getClassNames();
    E1ComponentObject* makeClassObject(const QString& qsName);

    void onObjectCreated();
    void onObjectDestroyed();

    void            setPlatformCapabilities(AppCapabilities caps) {m_Capabilities = caps;}
    AppCapabilities getPlatformCapabilities()                     {return m_Capabilities;}

private:
    static E1Component* m_pSelf;

    AppCapabilities m_Capabilities;

    E1ComponentObjectFactory* m_pFactory;

    int m_iObjectCount;
};

#endif // E1COMPONENT_H
