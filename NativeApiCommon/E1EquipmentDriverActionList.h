#ifndef E1EQUIPMENTDRIVERACTIONLIST_H
#define E1EQUIPMENTDRIVERACTIONLIST_H

#include "E1InterfaceItemList.h"
#include "E1EquipmentDriverAction.h"

class E1EquipmentDriverActionList
        : public E1InterfaceItemList<E1EquipmentDriverAction>
{
public:
    E1EquipmentDriverActionList(const E1ComponentObject &component) : E1InterfaceItemList(component) {}//qWarning();}
    virtual ~E1EquipmentDriverActionList() {}//qWarning();}

    QString xml(int indent = 1);
};

#endif // E1EQUIPMENTDRIVERACTIONLIST_H
