#ifndef E1EQUIPMENTDRIVER_H
#define E1EQUIPMENTDRIVER_H

#include "E1ComponentObject.h"
#include "E1EquipmentDriverActionList.h"
#include "E1EquipmentDriverParameterList.h"

class E1EquipmentDriver
        : public E1ComponentObject
{
public:
    E1EquipmentDriver(const QString& qsComName, long lVersion, int iType);
    virtual ~E1EquipmentDriver() {}

    enum EquipmentTypes {
        BarcodeScanner = 0,
        CardReader,
        FiscalRegister,
        ReceiptPrinter,
        LabelPrinter,
        CustomerDisplay,
        DataCollectionTerminal,
        AcquiringTerminal,
        ElectronicScales,
        LabelPrintingScales,
        CashRegisterOffline
    };

    QString getVersionString() const;

    void    setDescription(const QString& qsDescription, int iAlias = E1Tr::English) {m_Description.insert(iAlias, qsDescription);}
    QString getDescription() const {return E1Tr::inst().loc(m_Description);}

    void    setDownloadURL(const QString& qsUrl)   {m_qsUrl = qsUrl;}
    QString getDownloadURL() const                 {return m_qsUrl;}

    void        setLastError(const E1Exception& e) {m_LastError = e;}
    E1Exception getLastError() const               {return m_LastError;}

    virtual void    setDemoMode(bool bDemoMode)    {m_bDemoMode = bDemoMode;}
    virtual bool    isDemoMode() const             {return m_bDemoMode;}
    virtual QString getDemoModeDescription() const {return "";}

    static QString  getEquipmentType(int iType);

    virtual void    openConnection(QString& qsDeviceID);
    virtual void    closeConnection(const QString& qsDeviceID);
    virtual QString testDevice(const QString& /*qsDeviceID*/) {return "";}

    bool doAdditionalAction(const QString &qsActionName);

    // Interface functions
    virtual void getVersionString(    const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void getDescription(      const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void getLastError(        const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void getParameters(       const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void setParameter(        const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void open(                const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void close(               const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void deviceTest(          const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void getAdditionalActions(const E1Method& method, E1ValueList& parameters, E1Value& returnValue);
    virtual void doAdditionalAction(  const E1Method& method, E1ValueList& parameters, E1Value& returnValue);

protected:
    int                m_iType; // EquipmentTypes
    QMap<int, QString> m_Description;
    QString            m_qsUrl;
    bool               m_bDemoMode;

    E1Exception       m_LastError;

//    QList<QString>     m_DeviceIDs;
    E1EquipmentDriverParameterList m_Parameters;
    E1EquipmentDriverActionList    m_Actions;
};

#define ADD_FORM_PROPERTY(_name, _name_eng, _default_value, _caption_eng)                   \
    E1Property& _name = m_Properties.append(_name_eng);                                     \
    _name.setValue(_default_value);                                                         \
    E1EquipmentDriverParameter& _name##Parameter = m_Parameters.append(_name, _caption_eng)

#define ADD_FORM_PROPERTY_RO(_name, _name_eng, _default_value, _caption_eng)                \
    ADD_FORM_PROPERTY(_name, _name_eng, _default_value, _caption_eng);                      \
    _name.setWritable(false)

#define RUS_FORM_PROPERTY(_name, _name_loc, _caption_loc)                                   \
    m_Properties.setItemName(_name, _name_loc, E1Tr::Russian);                              \
    m_Parameters.setItemName(_name##Parameter, _name_loc, E1Tr::Russian);                   \
    _name##Parameter.setCaption(_caption_loc, E1Tr::Russian)

#define ENG_FORM_PROPERTY_ATTRIBUTES(_name, _page_caption_eng, _group_caption_eng, _description_eng, _choice_list_eng, _field_format_eng)   \
    E1EquipmentDriverParameter::Attributes _name##Attributes;                                                                               \
    _name##Attributes.PageCaption  = _page_caption_eng;                                                                                     \
    _name##Attributes.GroupCaption = _group_caption_eng;                                                                                    \
    _name##Attributes.Description  = _description_eng;                                                                                      \
    _name##Attributes.ChoiceList   = _choice_list_eng;                                                                                      \
    _name##Attributes.FieldFormat  = _field_format_eng;                                                                                     \
    _name##Parameter.setAttributes(_name##Attributes)

#define RUS_FORM_PROPERTY_ATTRIBUTES(_name, _page_caption_loc, _group_caption_loc, _description_loc, _choice_list_loc, _field_format_loc)   \
    E1EquipmentDriverParameter::Attributes _name##AttributesRus;                                                                            \
    _name##AttributesRus.PageCaption  = _page_caption_loc;                                                                                  \
    _name##AttributesRus.GroupCaption = _group_caption_loc;                                                                                 \
    _name##AttributesRus.Description  = _description_loc;                                                                                   \
    _name##AttributesRus.ChoiceList   = _choice_list_loc;                                                                                   \
    _name##AttributesRus.FieldFormat  = _field_format_loc;                                                                                  \
    _name##Parameter.setAttributes(_name##AttributesRus, E1Tr::Russian)

//#define ENG_FORM_PROPERTY_MASTER(_Master_Parameter_Name_eng, _Master_Parameter_Operation_eng, _Master_Parameter_Value_eng)

#define ADD_E1_ACTION(_class, _member, _name_eng, _caption_eng, _fires_external_event)      \
    E1EquipmentDriverAction& _member = m_Actions.append(_name_eng);                         \
    _member.setCaption(_caption_eng);                                                       \
    _member.setAction((action_function)&_class::_member);                                   \
    _member.firesExternalEvent(_fires_external_event);                                      \
    ADD_E1_FUNCTION(_class, _member##Direct, _name_eng)

#define RUS_ACTION(_member, _name_loc, _caption_loc)                                        \
    m_Actions.setItemName(_member, _name_loc, E1Tr::Russian);                               \
    _member.setCaption(_caption_loc, E1Tr::Russian);                                        \
    RUS_FUNCTION(_member##Direct, _name_loc)

#define E1_ACTION(_member)                                                                  \
    QString _member();                                                                      \
    virtual void _member##Direct(const E1Method& method, E1ValueList& parameters, E1Value& returnValue)

#define E1_ACTION_DIRECT(_class, _member)                                                   \
    }                                                                                       \
    void _class::_member##Direct(const E1Method& /*method*/, E1ValueList& /*parameters*/, E1Value& returnValue) \
    {                                                                                       \
        returnValue.setValue(_member())


#endif // E1EQUIPMENTDRIVER_H
