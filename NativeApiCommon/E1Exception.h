#ifndef E1EXCEPTION_H
#define E1EXCEPTION_H

#include <QMap>
#include <QString>

#include "E1Translator.h"

class E1Exception;
typedef E1Exception E1Ex;

class E1Exception
{
public:
    E1Exception(const E1Exception& e);
    E1Exception(int iCode = Unknown, const QString& qsDescriptionEn = "");

    enum ErrorCodes {
        Unknown      = 0,
        OutOfRange   = 1,
        NameNotFound = 2,
        UserError    = 1000
    };

    const int getCode() const {return m_iCode;}
    void      setDescription(const QString& qsDescription, int iAlias = E1Tr::English) {m_Description.insert(iAlias, qsDescription);}
    QString   getDescription() const                                                   {return E1Tr::inst().loc(m_Description);}

    void      clear();

    E1Exception& operator=(const E1Exception& right);

protected:
    int m_iCode;
    QMap<int, QString> m_Description;
};

#define EX_INIT(text, err_num) E1Ex e(err_num, text)
#define EX_RUS(text)           e.setDescription(text, E1Tr::Russian)
#define EX                     e

#endif // E1EXCEPTION_H
