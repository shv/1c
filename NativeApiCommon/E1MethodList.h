#ifndef E1METHODLIST_H
#define E1METHODLIST_H

#include "E1InterfaceItemList.h"
#include "E1Method.h"

class E1MethodList
        : public E1InterfaceItemList<E1Method>
{
public:
    E1MethodList(const E1ComponentObject& component) : E1InterfaceItemList(component) {}//qWarning();}
    virtual ~E1MethodList() {}//qWarning();}

    bool isFunction(long lIndex) const;
    long countParameters(long lIndex) const;
    bool readParameterDefaultValue(long lIndex, long lParameterIndex, tVariant* pvDefaultValue) const;

    bool execute(long lIndex, tVariant* pavParameters, long lArraySize);
    bool execute(long lIndex, tVariant* pvReturnValue, tVariant* pavParameters, long lArraySize);

private:
    void inputConversion(const E1Method& method, E1ValueList &destination, tVariant *pavSource, long lArraySize);
    void outputConversion(tVariant *pavDestination, E1ValueList &source);
};

#endif // E1METHODLIST_H
