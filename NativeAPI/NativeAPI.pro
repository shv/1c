CONFIG(release, debug|release){
    DEFINES += QT_NO_DEBUG_OUTPUT

    win32-msvc* {
        QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
    }
}

DEFINES += NATIVEAPI_QT_LIBRARY
DEFINES += _WINDOWS

win32-msvc* {
    DEFINES += "_BIND_TO_CURRENT_VCLIBS_VERSION=1"
}

HEADERS += \
    com.h \
    ComponentBase.h \
    IMemoryManager.h \
    types.h \
    NativeAPI_QT_Global.h \
    AddInDefBase.h \
    addinlib.h

OTHER_FILES += \
    addin.idl \
    MANIFEST.xsd

